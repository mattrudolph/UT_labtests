
"""mamba_analysis.py: Base analysis of the output from a Mamba run with actual signals.  If run as a script, specify the input directory and pedestal file on the command line.  Uses mambaproc as a base, implementing the proc_event method to look for signal clusters."""

from pythonbase.mambaproc import mambaproc
import ROOT
import pythonbase.pedestal as pedestal
from pythonbase.clusterizer import clusterize,clusterpos

class mambaanalysis(mambaproc):
    def __init__(self,datfiles,pedfile=None,plotdir=None,maskedchannels=None,maxevents=-1,startDelay=1, stopADC=37):
        mambaproc.__init__(self,datfiles,pedfile,plotdir,maskedchannels,maxevents,startDelay, stopADC)

        ##Clustering thresholds (in units of noise)
        self.thresh = 4
        self.lowthresh = 3

        self.noise_limit = 150 #to flag a channel as bad
        
        #ROOT histograms to look at signals
        self.allsig = ROOT.TH1F("signals","all signals; Cluster ADC",200,0,1000)
        self.allsn  = ROOT.TH1F("sovern","all s over n; S/N",200,0,60)
        self.hitmap = ROOT.TH1F("hitmap","hit channels; Channel;N_{hit}",512,-0.5,511.5)
        self.nhit   = ROOT.TH1F("nhit", "num hit per event; N_{hit}",10,-0.5,9.5)
        self.hitsize= ROOT.TH1F("hitsize","num strip per hit; N_{strip}",5,0.5,5.5)
        for i in range(1,5):
            self.hitsize.GetXaxis().SetBinLabel(i,"{0}".format(i))
        self.hitsize.GetXaxis().SetBinLabel(5,"5+")

        ##Study large (>3strips) clusters separately
        self.largesize= ROOT.TH1F("hitsize_large","num strip per hit; N_{strip}",100,0.5,100.5)
        self.largemap = ROOT.TH1F("hitmap_large","map of large clusters (>3strips);Channel;N_{hit} 4+ strips",512,-0.5,511.5)
        self.largesig = ROOT.TH1F("signal_large","signals for large clusters; Cluster ADC",200,0,2000)


        ##Timing plots
        self.tdc = ROOT.TH1F("tdc","tdc time; TDC", 10, 0.5, 10.5)
        self.sigvtdc = ROOT.TProfile("sigvtdc","sig v tdc time; TDC; <ADC>",10, 0.5, 10.5)
        self.fullsigvtdc = ROOT.TH2F("fullsigvtdc","2d hist of sig v tdc time; TDC; ADC", 10,0.5,10.5,100,0,1000)

        #Study balance of charge (eta) for clusters
        self.hiteta = ROOT.TH1F("hiteta","hit eta; #eta",100,0,1)

        #Study left right imbalance due to cross talk effects
        self.leftright = ROOT.TProfile("leftright","left channel - right channel v. center channel ADC;ADC;(ADC_{left})-(ADC_{right})",20,100,500)


    def proc_event(self, ev):

        ##First unpack the event info from the event tuple

        #TDC time
        tdc = ev[0][-1]


        #Fill in the headers
        headers = ev[1]
        for i,h in enumerate(headers):
            self.headers.Fill( i, h)
            self.headerdists[ i%16].Fill(h)
        
        data=ev[2]

        #Save the raw ADC for interesting channels
        for chan,hist in self.rawmonitor.iteritems():
            hist.Fill( data[chan] )
        
        #subtract pedestal from each channel for this event
        pedsub =  [ data[i] - self.ped[i] for i in range(len(data))]
            
        #subtract the common mode noise from each block of 32 channels
        cmsub, cmshifts = pedestal.commonmode( pedsub, channelmask=self.channelmask )

        #Save the common mode subtracted signals for interesting channels
        for chan,hist in self.cmsmonitor.iteritems():
            hist.Fill( cmsub[chan] )

        #Fill the actual common mode shifts
        for i,s in enumerate(cmshifts):
            self.cmshifts[i].Fill(s)

        #Raw left - right plot
        for i,c in enumerate(cmsub):
            if i > 0 and i < len(cmsub)-1 and self.channelmask[i] and self.polarity*c > self.thresh*self.noise[i]:
                self.leftright.Fill( self.polarity*c, self.polarity*cmsub[i-1] - self.polarity*cmsub[i+1] )

            
        #make a list of clusters of strips ( each cluster is just a list of channel numbers that are included)
        clusters = clusterize( cmsub, self.noise , channelmask = self.channelmask, high = self.thresh, low = self.lowthresh, polarity = self.polarity)

        #fill histogram of clusters per event
        self.nhit.Fill(len(clusters))

        #loop over all the clusters
        for c in clusters:
            size = len(c)

            if size > 4:
                self.hitsize.Fill(5)
            else:
                self.hitsize.Fill(size)

           
            #total signal and signal/noise
            totsig=0
            totsn=0


            #loop over every strip in the cluster
            for strip in c:
                #add up the signals
                totsig += self.polarity*cmsub[strip]
                totsn += self.polarity*cmsub[strip]/self.noise[strip]

                
            #remove huge clusters that seem to be due to large fluctuations of entire beetle block
            if size > 3:
                self.largesize.Fill(size)
                pos = clusterpos(c,cmsub)
                self.largemap.Fill(pos)
                self.largesig.Fill(totsig)
                continue
 

            
                
            #fill the time information
            self.tdc.Fill(tdc)
            self.sigvtdc.Fill(tdc,totsig)
            self.fullsigvtdc.Fill(tdc,totsig)
            
            
            # #cut on time region with most signal
            # if (tdc < 20) or (tdc > 60):
            #     continue

            #for two strip clusters, plot fraction of charge on the right strip
            if size == 2:
                self.hiteta.Fill( cmsub[c[1]]/(cmsub[c[0]]+cmsub[c[1]]) )
            #fill histograms
            self.allsig.Fill(totsig)
            self.allsn.Fill(totsn)

            pos = clusterpos(c,cmsub)
            self.hitmap.Fill(pos)


    def plot_hits(self):
        #make sure canvas is active
        self.canv.cd()

        #draw on the canvas, then save
        self.allsig.Draw()
        self.saveplot("allsignals",self.allsig)

        self.allsn.Draw()
        self.saveplot("sovern",self.allsn)

        self.hitmap.Draw()
        self.saveplot("hitmap",self.hitmap)
        self.nhit.Draw()
        self.saveplot("nhits",self.nhit)
        self.hitsize.Draw()
        self.saveplot("hitsize",self.hitsize)

        self.hiteta.Draw()
        self.saveplot("hiteta",self.hiteta) 


        self.leftright.SetStats(False)
        self.leftright.Draw()
        self.saveplot("leftright",self.leftright)

        if not all(self.channelmask):
            #only connected channels plots
            nunmasked = sum(self.channelmask)
            nmasked = 512 - nunmasked

            unmasked = [ c for c in range(512) if self.channelmask[c] ]
            
            hitmap_conn = ROOT.TH1F("hitmap_conn","hitmap; Strip; Hits",nunmasked,-0.5,nunmasked-0.5)
            largemap_conn = ROOT.TH1F("hitmap_large_conn","hitmap; Strip; Hits",nunmasked,-0.5,nunmasked-0.5)
            for i,b in enumerate(unmasked):
                hitmap_conn.SetBinContent( i+1, self.hitmap.GetBinContent( b+1) )
                largemap_conn.SetBinContent( i+1, self.largemap.GetBinContent( b+1) )

            hitmap_conn.Draw()
            self.saveplot("hitmap_conn",hitmap_conn)
            largemap_conn.Draw()
            self.saveplot("hitmap_large_conn",largemap_conn)
        
    def plot_largehits(self):    
        self.largesize.Draw()
        self.saveplot("hitsize_large",self.largesize)
        self.largemap.Draw()
        self.saveplot("hitmap_large",self.largemap)
        self.largesig.Draw()
        self.saveplot("signals_large",self.largesig)

    def plot_timing(self):
        self.tdc.Draw()
        self.saveplot("tdc",self.tdc)      
        self.sigvtdc.Draw()
        self.saveplot("sigvtdc",self.sigvtdc)

        maxbin = self.sigvtdc.GetMaximumBin()
        if maxbin==1:
            low=1
            high=3
        elif maxbin==10:
            low=8
            high=10
        else:
            low=maxbin-1
            high=maxbin+1

        self.canv.SetRightMargin(0.16)
        self.fullsigvtdc.Draw("COLZ")
        self.saveplot("fullsigvtdc",self.fullsigvtdc)
        self.canv.SetRightMargin(0.05)
        
        selsig = self.fullsigvtdc.ProjectionY("selsig",low,high)
        selsig.Draw()
        self.saveplot("selsignals",selsig)
        
    def sum_plots(self):
        self.plot_pedestal()
        self.plot_cmshifts()
        self.plot_headers()
        self.plot_interesting()
        
        self.plot_hits()
        self.plot_largehits()
        self.plot_timing()

        
#########################            
if __name__== '__main__':
    import sys

    if len(sys.argv) < 2:
        print "Pass the dat file"
        sys.exit()

    infiles = sys.argv[1:]
    pedfile = None

    sys.argv.append('-b')

    ROOT.gROOT.Macro("~/rootlogon.C")


    print bondedchans, unbondedchans
    
    plotter = rsanalysis(infiles[0],pedfile)

    
    plotter.maskchannels(unbondedchans)
    plotter.maskchannels( [0,1,6,420,421]) #first and last connected channel
    
    #investigate ADCs for channels producing strange hits
    plotter.addinterestingchannels( [1, 6, 7, 145, 0, 420, 421, 240] )

    print "DEBUG: before flag channels"
    
    plotter.flag_channels()
    print "DEBUG: after flag channels"
    
    plotter.plot_pedestal()

    plotter.loop_file()

    for f in infiles[1:]:
        plotter.datfile = f
        plotter.ped,plotter.noise = pedestal.computepedestal(f,ismamba=True,channelmask=plotter.channelmask)
        plotter.loop_file()
    
    plotter.sum_plots()
