#!/usr/bin/env python

"""
Basic analysis of all files from an aliscan laser run

To run, execute:
python baseplots.py path/to/directory

Defines baseplotter class inheriting from aliproc.  Creates the following histograms in the aliproc framework:
hits: channel numbers with hits (signal > 5*noise) in each individual run
sigs: ADC signals for hit channels in each individual run
hitsall: channel numbers with hits for all runs combined

Additionally defines the following methods:
plot_pedestal: creates histograms and plots the computed pedestal and noise
sum_plot: Saves hitsall plot after all runs completed

The main script is used to run the plotter
"""


from pythonbase.aliproc import aliproc
import ROOT
import pythonbase.pedestal as pedestal

class baseplotter(aliproc):
    def __init__(self,*indirs):
        aliproc.__init__(self,*indirs)

        self.hits = ROOT.TH1F("hits","hits",256,-0.5,255.5)
        self.sigs = ROOT.TH1F("sigs","sigs",512,-512,512)

        self.hitsall = ROOT.TH1F("hitsall","hits",256,-0.5,255.5)
        self.sigsall = ROOT.TH1F("sigsall","sigs",512,-512,512)

        ##set this to true if you want run-by-run plots
        self.summaryonly = False

    def begin_file(self, j):
        self.hits.Reset()
        self.sigs.Reset()

    def proc_event(self,ev):

        data = ev[-1]
        pedsub =  [ data[i] - self.ped[i] for i in range(len(data))]
        cmsub,_ = pedestal.commonmode( pedsub )

        for c,v in enumerate(cmsub):
            if self.polarity*v > 5*self.noise[c]:
                self.hits.Fill(c)
                self.hitsall.Fill(c)
                self.sigs.Fill(v)
                self.sigsall.Fill(v)

    def end_file(self,j):
        if not self.summaryonly:
            self.canv.cd()

            self.hits.Draw()
            self.saveplot("hits_%04i" % j,self.hits)
            self.sigs.Draw()
            self.saveplot("sigs_%04i" % j,self.sigs)

    def sum_plot(self):
        self.canv.cd()
        self.hitsall.Draw()
        self.saveplot("hits_all",self.hitsall)
        self.sigsall.Draw()
        self.saveplot("sigs_all",self.sigsall)

    
if __name__== '__main__':
    import sys

    if len(sys.argv) < 2:
        print "Pass a directory to process as the first argument"
        sys.exit()

    indirs = sys.argv[1:]
    
    ##put ROOT in batch mode for no graphics
    sys.argv.append('-b')

    plotter = baseplotter(*indirs)
    #plotter.summaryonly = True
    plotter.plot_pedestal()

    plotter.loop_files()
    
    plotter.sum_plot()
