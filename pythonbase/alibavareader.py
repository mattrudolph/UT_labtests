#!/usr/bin/env python

"""
alibavareader.py

Implements the decoder for the alibava binary data file.  Mirrors implementation in mambareader.py.  prepfile is required before calling next_event; it will return some file header information.

Usual use:
call next_event( inputfile object ) until None is returned

next_event returns a 3 tuple:
* first index is another tuple with event info, the most important of which is the tdc time in the last spot
* second index are all the beetle headers
* third index are all the data channels
"""

from struct import *


def parse_datablock( data ):
    """Read the event info and all the short int in one event's data block"""
    try:
        udata = unpack('d I I H 288H',data)
    except:
        return None

    value = udata[0]
    clock = udata[1]
    time = udata[2]

    ipart = (time & 0xffff0000) >> 16
    fpart = (time & 0xffff)
    #print time, ipart, fpart
    tdc = 100.0*( ipart + float(fpart)/(65535.0) )
    
    temp = udata[3]
    header = udata[4:20] + udata[148:164]
    data =  udata[20:148] + udata[164:] 
    
    return value, clock, tdc, temp, header, data


class reader:
    def __init__(self,infile):
        self.f = infile

        self.prepfile()


    #For alibava, this actually reads the file info at the beginning of the file and returns it
    def prepfile( self ):
        """Read the alibava file header info and return it -- doing so prepares the file to read the event data blocks"""
        #apparently the time is time_t and *NOT* uint32 like the manual says
        b = self.f.read(8)
        timestamp = unpack('q',b)[0]

        b= self.f.read(4)
        runtype = unpack('i',b)[0]

        b = self.f.read(4)
        headlength = unpack('I',b)[0]

        b = self.f.read(headlength)
        fullinfo = unpack( str(headlength) + 's', b)[0]
        try:
            info = fullinfo[:fullinfo.index('\x00')]
        except:
            print "Failed to truncate fullinfo"
            info = fullinfo

        b = self.f.read(256*8)
        pedestal = unpack( '256d', b)

        b = self.f.read(256*8)
        noise = unpack( '256d', b)

        #pedestal and noise returned here is calculated by alibava and may not agree with our calculation
        return timestamp, runtype, info, pedestal, noise

    def resetfile( self ):
        self.f.seek(0)
        self.prepfile()
    
    def read_block( self ):
        
        b = self.f.read(4)
        fullblocktype = unpack('I',b)[0]

        #check that it is as expected
        if (fullblocktype & 0xffff0000) != 0xcafe0000:
            print "Bad format for start of datablock header"
            blocktype = 0
        else:
            blocktype = fullblocktype & 0x0000ffff
    
        b = self.f.read(4)
        size = unpack('I',b)[0]

        data = self.f.read(size)
        
        return blocktype, data
    
    #search for next data block, and return the channel values as a list of 256 channel
    def next_event( self ):
        """Return the next event in the file, returning a tuple of the event info, the beetle header, and the channel data"""
        btype = 10
        while btype != 2:
            try:
                (btype, data) = self.read_block()
            except:
                return None

        (value, clock, tdc, temp, header, data) = parse_datablock(data)

        #return a tuple with alibava specific event info stuff, then the beetle header, then the data
        #tdc is most useful extra value; both here and for mamba it will be at the end of the first return value (i.e. next_event [0][-1] )
        return ((value,clock,temp,tdc), header, data)
