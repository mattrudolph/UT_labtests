#!/usr/bin/env python

"""
Base class to loop over mamba board output in python

This base class will loop over all events in a given datfile.  It will use the given pedestal file to compute the pedestal and noise.  Calling loop_file will then loop over the file, calling proc_event(ev) for each event.

To use this class, you must inherit from it, call the mambaproc __init__ method, and provide the proc_event(ev) method to perform the desired analysis

See implementation example in UT_labtests/mambabaseplots.py
"""

import os
import re
import ROOT
from pedestal import *
import mambareader


headernames = [ 'I0','I4', 'P1', 'P0', 'I1','I5', 'P3', 'P2', 'I2', 'S0', 'P5', 'P4', 'I3', 'S1', 'P7','P6']

class mambaproc:
    def __init__(self, datfiles, pedfile, plotdir=None, maskedchannels=None,maxevents=-1, startDelay=1, stopADC=37):
        self.datfiles = datfiles
        if pedfile:
            self.pedfile = pedfile
        else:
            #Just use the first file as the pedestal if not one specified
            self.pedfile = datfiles[0]

        #Create a plotdirectory
        if plotdir is None:
            self.plotdir = "plots/" + os.path.basename(datfiles[0])[:-4] + '/'
        else:
            self.plotdir = plotdir

        if not os.path.exists(self.plotdir):
            os.makedirs(self.plotdir)

        self.maxevents = maxevents #max events to loop over in one file

        ##Masked channels are not included in Common mode subtraction, and no clusters can be found there
        self.channelmask = [True,]*512
        if maskedchannels:
            self.maskchannels(maskedchannels)

        #print "Channel mask =",self.channelmask
            
        ##Polarity of the signals -- set to -1.0 for p-type sensors
        self.polarity=1.0


        ##Mamba ADC start/stop; needed by the reader
        self.startDelay=startDelay
        self.stopADC=stopADC
        
        ##Compute the pedestal
        try:
            print "Calculate pedestal with", self.pedfile
            with open(self.pedfile) as pf:
                pedreader = mambareader.reader(pf, startDelay, stopADC)
                self.ped,self.noise = computepedestal(pedreader,channelmask=self.channelmask,maxevents=self.maxevents)
        except:
            print "Pedestal calculation failed"
            self.ped = None
            self.noise = None
            raise

        #ROOT canvas for saving plots
        self.canv = ROOT.TCanvas()
        #ROOT file for plots
        self.outfile = ROOT.TFile(self.plotdir + "histograms.root","RECREATE")

        ###################################
        #Basic plots most analyses will use
        ###################################
        
        #Common mode shifts per block of 32 channels
        self.cmshifts = [ ROOT.TH1F("cmshift{0}".format(i),"CM shift; Common mode shift [ADC]",200,-500,500) for i in range(16)]

        #Profile plot of each header for each block
        self.headers = ROOT.TProfile("headers","header value mean and std dev;16*(Block #)+Header #; ADC",64,-0.5,63.5,'S')

        #Actual distributions for the headers
        self.headerdists = [ ROOT.TH1F("header{0}".format(headernames[i]),"header; ADC {0}".format(headernames[i]),200,-1000,1000) for i in range(16)]

        #Dictionaries for plots to keep track of all ADC on "interesting" channels
        self.rawmonitor = {}
        self.cmsmonitor = {}

        
    def maskchannels(self, masklist):
        """Add the given masklist to the channelmask"""
        for m in masklist:
            self.channelmask[m] = False


    def flag_channels(self):
        """Flag noisy or bad channels to remove from analysis with channel mask"""
                
        didflag = False

        for i,n in enumerate(self.noise):
            if n > self.noise_limit:
                didflag = True
                self.maskchannels( [i,] )

        #redo the pedestal without the flagged channels
        if didflag:
            self.ped,self.noise = computepedestal(self.pedfile,ismamba=True,channelmask=self.channelmask)
            
    def addinterestingchannels( self, chanlist ):
        """Add the given channel list to the dictionary of channels for which we monitor all ADCs"""
        for c in chanlist:
            self.rawmonitor[c] = ROOT.TH1F( "rawadc_chan{0}".format(c), "Raw channel ADC; ADC", 200, -1000,1000)
            self.rawmonitor[c].SetStats(True)
            self.cmsmonitor[c] = ROOT.TH1F( "cmsadc_chan{0}".format(c), "CMS channel ADC; ADC", 200, -1000,1000)
            self.cmsmonitor[c].SetStats(True)



    #####################################
    ##Functions to deal with saving plots
    #####################################    
    def saveplot(self,plotname,obj=None):
        """Save the canvas with the given plotname; obj if given is saved to the rootfile"""
        if obj:
            self.outfile.cd()
            obj.Write()
        self.canv.SaveAs( self.plotdir + plotname + ".png")
        self.canv.SaveAs( self.plotdir + plotname + ".pdf")

    def plot_pedestal(self):
        """Plot the pedestal and noise"""
        if self.ped is None or self.noise is None:
            print "WARNING: no pedestal or noise found"
            return
        
        hped = ROOT.TH1F("pedestal","pedestal;Channel;Pedestal [ADC]",512,-0.5,511.5)
        hnoise = ROOT.TH1F("noise","noise;Channel;CMS Noise [ADC]",512,-0.5,511.5)
        for i,n in enumerate(self.ped):
            if i==hped.GetNbinsX():
                break
            hped.SetBinContent(i+1, self.ped[i])
            hnoise.SetBinContent(i+1,self.noise[i])

        self.canv.cd()
        hped.Draw("HIST")
        self.saveplot("pedestal",hped)
    
        hnoise.Draw("HIST")
        self.saveplot("noise",hnoise)

        if not all(self.channelmask):
            nunmasked = sum(self.channelmask)
            nmasked = 512 - nunmasked

            #plot unmasked
            hped_conn = ROOT.TH1F("pedestal_conn","pedestal;Strip;Pedestal [ADC]",nunmasked,-0.5,nunmasked - 0.5)
            hnoise_conn = ROOT.TH1F("noise_conn","pedestal;Strip;CMS Noise [ADC]",nunmasked,-0.5,nunmasked - 0.5)
            unmasked = [ c for c in range(512) if self.channelmask[c] ]
            for i,b in enumerate(unmasked):
                hped_conn.SetBinContent( i+1, hped.GetBinContent( b+1) )
                hnoise_conn.SetBinContent( i+1, hnoise.GetBinContent( b+1) )
        
            hped_conn.Draw("HIST")
            self.saveplot("pedestal_conn",hped_conn)
    
            hnoise_conn.Draw("HIST")
            self.saveplot("noise_conn",hnoise_conn)

        
            #plot unconnected
            hped_unconn = ROOT.TH1F("pedestal_unconn","pedestal;Strip;Pedestal [ADC]",nmasked,-0.5,nmasked-0.5)
            hnoise_unconn = ROOT.TH1F("noise_unconn","pedestal;Strip;CMS Noise [ADC]",256,-0.5,nmasked-0.5)
            masked = [ c for c in range(512) if not self.channelmask[c] ]
            for i,b in enumerate(masked):
                hped_unconn.SetBinContent( i+1, hped.GetBinContent( b+1) )
                hnoise_unconn.SetBinContent( i+1, hnoise.GetBinContent( b+1) )
        
            hped_unconn.Draw("HIST")
            self.saveplot("pedestal_unconn",hped_unconn)
    
            hnoise_unconn.Draw("HIST")
            self.saveplot("noise_unconn",hnoise_unconn)

    def plot_cmshifts(self):
        """Plot the distribution of common mode shifts for each block of 32 channels"""
        
        txtsig=ROOT.TPaveText(0.7,0.22,0.9,0.9,"NDC")
        txtsig.SetBorderSize(0)
        txtsig.SetFillColor(0)
        txtm=ROOT.TPaveText(0.2,0.22,0.4,0.9,"NDC")
        txtm.SetBorderSize(0)
        txtm.SetFillColor(0)

        for i,h in enumerate(self.cmshifts):
            h.SetStats(False)
            h.SetLineColor(i+1)
            txtm.AddText( "#mu_{{ {0} }} = {1:.2f}".format(i,h.GetMean()) )
            txtsig.AddText( "#sigma_{{ {0} }} = {1:.2f}".format(i, h.GetStdDev() ) )
            self.outfile.cd()
            h.Write()
            if i==0:
                h.Draw()
            else:
                h.Draw("SAME")
        txtm.Draw()
        txtsig.Draw()
        self.saveplot("cmshifts")
            
    def plot_headers(self,plotall=False):
        """Plot the header summary, or all raw values if plotall is True"""
        
        self.canv.cd()
        
        self.headers.SetStats(False)
        self.headers.Draw()
        self.headers.GetYaxis().SetRangeUser(-800,800)
        self.canv.lines = []
        for i in range(15):
            x = i*4 + 3.5
            self.canv.lines.append( ROOT.TLine( x, -800, x, 800))
            if( (i+1) % 4 == 0 ):
                self.canv.lines[-1].SetLineColor(ROOT.kRed)
            self.canv.lines[-1].Draw()
        self.saveplot("headers",self.headers)

        if plotall:
            for i,h in enumerate(self.headerdists):
                h.Draw()
                self.saveplot("header{0}".format(headernames[i]),h)

                
    def plot_interesting(self):
        """Plot the ADCs of the channels flagged as interesting"""
        self.canv.SetLogy()
        ROOT.gStyle.SetOptStat(1111)
        
        for h in self.rawmonitor.itervalues():

            h.Draw()

            ##Take out fitting for now
            # h.Fit('gaus','','',-500,500)
            # f = h.GetListOfFunctions().FindObject('gaus')
            
            # txt = ROOT.TPaveText(0.7,0.7,0.9,0.9,'NB NDC')
            # txt.SetFillColor(0)
            # txt.AddText( "#mu = {0:.2f} #pm {1:.2f}".format( f.GetParameter(1), f.GetParError(1)) )
            # txt.AddText( "#sigma = {0:.2f} #pm {1:.2f}".format( f.GetParameter(2), f.GetParError(2)) )
            # txt.Draw()
            self.saveplot( h.GetName(), h)
        for h in self.cmsmonitor.itervalues():
            h.Draw()
            self.saveplot( h.GetName(), h)

        ROOT.gStyle.SetOptStat(0000)
        self.canv.SetLogy(False)
            
    def loop_files(self):
        """Loop over all files and call loop_file on them"""
        for f in self.datfiles:
            self.loop_file(f)
        
    def loop_file(self,datfile):
        """Loop over data file and process all events"""
        with open(datfile) as f:
            reader = mambareader.reader(f,self.startDelay,self.stopADC)
            
            ev = reader.next_event()
            numproc = 0
            while ev is not None and (self.maxevents < 0 or numproc < self.maxevents):
                numproc += 1
                self.proc_event(ev)
                ev = reader.next_event()

            print "Processed", numproc,"events"
