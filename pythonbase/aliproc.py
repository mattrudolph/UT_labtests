#!/usr/bin/env python

"""
Base class to loop over aliscan output in python

Input to constructor is a directory containing the aliscan output files.  This base class will identify all the available run files, compute the pedestal and noise from a separate pedestal run, and provides methods both to loop over all files, and to loop over all events in each file.  

To use, you must inherit this class, call the aliproc __init__ method, and provide the following methods:

begin_file(j): actions to take at the beginning of a new file with index number j.  For example, reset histograms that are filled for each file

proc_event( ev ): process a new event ev (as returned by alibavareader.next_event), and carry out whatever analysis is desired

end_file(j): actions to take after looping over a file with index number j.  For example, saving histogram plots for that file.

See implementation example in UT_labtests/baseplots.py
"""

import os
import re
import ROOT
from pedestal import *
from alibavareader import *

headernames = [ 'I0','I4', 'P1', 'P0', 'I1','I5', 'P3', 'P2', 'I2', 'S0', 'P5', 'P4', 'I3', 'S1', 'P7','P6']

class aliproc:
    def __init__(self,*indirs): ##Can call with one path, or many paths
        ##Input directory where the datfiles live


        print indirs
        self.indirs = list(indirs)
        print self.indirs
        self.scannums = []# [ int(re.findall("[0-9][0-9][0-9][0-9]",indir)[-1]) for indir in indirs]

        if not len(indirs):
            return

        ##Get the module name from the first directory (no error checking for different modules)
        ## pattern should be modname/LaserScan/scannumber for input directories
        self.modname = os.path.normpath(self.indirs[0]).split('/')[-3]

        ##save plots here
        firstnum = 0#min(self.scannums)
        lastnum= 1#max(self.scannums)
        plotdirname = self.modname
        for n in sorted(self.scannums):
            plotdirname += "_%04i" % n
        
        self.plotdir = os.path.join( 'plots', plotdirname )
        if not os.path.exists(self.plotdir):
            os.makedirs(self.plotdir)

        #ROOT canvas for saving plots
        self.canv = ROOT.TCanvas()
        #ROOT file for plots
        try:
            self.outfile = ROOT.TFile( os.path.join(self.plotdir, self.hfilename),"RECREATE")
        except:
            self.outfile = ROOT.TFile( os.path.join(self.plotdir, "histograms.root"),"RECREATE")

        ##Full list of datfiles we find -- later will also save their x,y unidex positions
        self.datfiles = []
        for indir in self.indirs:
            self.datfiles.extend( sorted([os.path.join(indir,f) for f in os.listdir(indir) if re.match("log[0-9]+\.dat",f) is not None]))
        self.xfiles = []
        self.yfiles = []
        self.nfiles = len(self.datfiles)

        for f in self.datfiles:
            ##Get the directory of the file
            fdir = os.path.dirname( f )
            ##Get the number of the run in the directory
            iternum = int( re.search(r"(log)([0-9]+)",f).group(2) )
            # The aliscan output console keeps track of real positions
            with open(os.path.join(fdir,'console.out')) as console:
                #find the iteration number in the console
                line = console.readline()
                while re.match("### SCAN ITERATION: %i" % iternum, line) is None:
                    line = console.readline()

                #Save the x,y position as logged (on line just after SCAN ITERATION)
                line = console.readline()
                [x,y] = [float(pos) for pos in re.findall("[0-9]+\.[0-9]",line)]
                self.xfiles.append(x)
                self.yfiles.append(y)
                
        ##Polarity of the signals -- change in your script if p-type
        self.polarity = 1.0

        ##Compute the pedestal
        try:
            self.ped,self.noise = computepedestal(os.path.join(self.indirs[0], 'pedestal.dat'))
        except:
            self.ped = None
            self.noise = None

        #Profile plot of each header for each block
        self.headers = ROOT.TProfile("headers","header value mean and std dev;16*(Block #)+Header #; ADC",64,-0.5,63.5,'S')

        #Actual distributions for the headers
        self.headerdists = [ ROOT.TH1F("header{0}".format(headernames[i]),"header; ADC {0}".format(headernames[i]),200,-1000,1000) for i in range(16)]


    #####################################
    ##Functions to deal with saving plots
    #####################################    
    def saveplot(self,plotname,obj=None):
        """Save the canvas with the given plotname; obj if given is saved to the rootfile"""
        if obj:
            self.outfile.cd()
            obj.Write()
        self.canv.SaveAs( os.path.join(self.plotdir, plotname + ".png"))
        self.canv.SaveAs( os.path.join(self.plotdir, plotname + ".pdf"))

    def plot_pedestal(self):
        """Plot the pedestal and noise"""
        if self.ped is None or self.noise is None:
            print "WARNING: no pedestal or noise found"
            return
        
        hped = ROOT.TH1F("pedestal","pedestal;Channel;Pedestal [ADC]",256,-0.5,255.5)
        hnoise = ROOT.TH1F("noise","noise;Channel;CMS Noise [ADC]",256,-0.5,255.5)
        for i,n in enumerate(self.ped):
            if i==hped.GetNbinsX():
                break
            hped.SetBinContent(i+1, self.ped[i])
            hnoise.SetBinContent(i+1,self.noise[i])

        self.canv.cd()
        hped.Draw("HIST")
        self.saveplot("pedestal",hped)
    
        hnoise.Draw("HIST")
        self.saveplot("noise",hnoise)

    def plot_headers(self,plotall=False):
        """Plot the header summary, or all raw values if plotall is True"""
        
        self.canv.cd()
        
        self.headers.SetStats(False)
        self.headers.Draw()
        self.headers.GetYaxis().SetRangeUser(-800,800)
        self.canv.lines = []
        for i in range(15):
            x = i*4 + 3.5
            self.canv.lines.append( ROOT.TLine( x, -800, x, 800))
            if( (i+1) % 4 == 0 ):
                self.canv.lines[-1].SetLineColor(ROOT.kRed)
            self.canv.lines[-1].Draw()
        self.saveplot("headers",self.headers)

        if plotall:
            for i,h in enumerate(self.headerdists):
                h.Draw()
                self.saveplot("header{0}".format(headernames[i]),h)

    #####################################
    ##Functions to loop over files
    #####################################  
    def loop_files(self,flist=None):
        """Loop over all data files in the directoryies to be analyzed"""
        if not flist:
            flist = range(self.nfiles)
        for j in flist:
            #now open the data file and process it
            self.begin_file(j)
            self.loop_file(j)
            self.end_file(j)

    def loop_file(self,j):
        """Loop over an individual file with index j"""
        with open(self.datfiles[j]) as f:
            (timestamp, runtype, info, pedestal, noise) = prepfile(f)
            
            ev = next_event(f)

            while ev is not None:

                self.proc_event(ev)
                ev = next_event(f)

        
