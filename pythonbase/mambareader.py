#!/usr/bin/env python

"""
mambareader.py

Implements the decoder for the mamba binary data file.  Mirrors implementation in alibavareader.py.  prepfile here does not do anything -- but it does in alibavareader. 

Usual use:
call next_event( inputfile object ) until None is returned

next_event returns a 3 tuple:
* first index is another tuple with event info, the most important of which is the tdc time in the last spot
* second index are all the beetle headers
* third index are all the data channels
"""


from struct import *
import sys

def find_beetle_header( blockadcs ):
    """Find the beetle header in the ADCs by looking for four high value channels"""
    thr = 80
    base = blockadcs[0]
    comp = [ abs(b - base) for b in blockadcs ]
    
    for i in range(1,len(comp)-3):
        if comp[i] > thr and comp[i+1] > thr and comp[i+2] > thr and comp[i+3] > thr:
            return i
    
    return -1

class reader:
    def __init__(self,infile, startDelay=1, stopADC=37):
        self.f = infile
        self.nbeetles = 4
        self.nsubsets = 4
        self.nchan = 32
        
        self.startDelay=startDelay
        self.stopADC=stopADC

        self.nADCs =  ( self.stopADC - self.startDelay - 1)*2
        self.totadcs = self.nbeetles*self.nsubsets*self.nADCs

        self.debug = False


    #This decoder doesn't do anything at the beginning of the file -- included so interface is close to alibavareader
    def prepfile( self ):
        return None

    def resetfile( self ):
        self.f.seek(0)

    def find_header( self ):
        """Find the Mamba header that indicates start of the next event"""
        #I'm not sure how but this seems not to get stuck at end of file
        while(True):
            b = self.f.read( 16 )
            heads = unpack('4I',b)

            if( (heads[0],heads[1],heads[2])  ==  (0xF1CA600D, 0x0, 0xFFFFFFFF) ):
                return
            else:
                self.f.seek( -15, 1)
     
    def check_footer( self ):    
        self.f.seek( ( 12 + self.totadcs/2 )*4, 1)
        b = self.f.read( 16 )
        foots = unpack('4I',b )
        self.f.seek( -( 16 + self.totadcs/2 )*4, 1)
        result = ( foots == (0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF) )
        if not result:
            print "Unexpected footer", format(foots[0],'10X'), format(foots[1],'10X'), format(foots[2],'10X'), format(foots[3],'10X')
            
        return result

    def read_evinfo( self ):
        b = self.f.read( 48 )
        u = unpack('12I',b)

        return tuple([ u[i+1] + (u[i] << 32) for i in range(0,12,2)])


    def read_adcs( self ):
        """Read in all the ADC values in the event data"""

        if self.debug:
            print "Read ADCs for an event"
        
        b = self.f.read( self.totadcs*2)
        try:
            ra = unpack('H'*self.totadcs,b)
        except:
            print "Failed to read enough bytes to get all ADCs"
            return None

        if self.debug:
            print "Unpacked all ADCs for an event"
        
        #data for a channel is separated by nbeetle*nsubset:
        # i = ichan*nbeetle*nsubset + ibeetle*nsubset + iXformSubset
        # where for some reason you transform the block number 0->1, 1->0, 1->3, 3->2
        #beetle 0 block 0
        startindices = [ 1, 0, 3, 2, 5, 4, 7, 6, 9, 8, 11, 10, 13, 12, 15, 14, ]
        header = []
        data = []
        debugGood = False
        for i in startindices:
            #adcs in 1 beetle or block
            adcs = [ ra[j] - 2048 for j in range(i,len(ra),16) ]
            #print len( adcs), ":", adcs

            if self.debug:
                print "Try to find header for block", i
            
            hpos = find_beetle_header( adcs )

            if hpos < 0:
                print "ERROR: Failed to find a beetle header for block", i
                print "    Full adc for block follows:"
                print "     ",adcs

                header += [0.]*4
                data += [0.]*self.nchan
                continue
                
            elif debugGood:
                print "DEBUG: full adc for a good block follows:"
                print adcs
                print "Header starts at", hpos
                debugGood = False
                
            header += adcs[ hpos:hpos+4]
            data += adcs[hpos+4: hpos+4+self.nchan]


        if self.debug:
            print "Read TDC time"
        #after the event we can find the footer and tdc time
        b = self.f.read( 16 ) #dont care about the footer (already was checked)
        b = self.f.read( 16 )
        tu = unpack('IIII', b)
        tdc = (   (tu[3] & 0xFF) - (tu[3]>>8) & 0xFF ) % 0xFF
        
        return (tdc, header,data)
            

    #Return data for the next event in the file
    def next_event( self ):
        """Get the next event in the file"""
        
        try:
            self.find_header( )
        except:
            print "Cannot find another Mamba header in the file"
            return None

        try:
            foot = self.check_footer( )
            if not foot:
                print "Skipping event with bad footer"
        except:
            print "Failed to read footer"
            return None

        try:
            evinfo = self.read_evinfo( )
        except:
            print "Did not read event info"
            return None

        try:
            (tdc, header,data) = self.read_adcs( )
        except:
            print "Did not read adcs"
            return None

        #if the footer was bad, try to read the next event
        if not foot:
            return self.next_event()
    
        #return a tuple with mamba specific event info stuff, then the beetle header, then the data
        #tdc is most useful extra value; both here and for alibava it will be at the end of the first return value (i.e. next_event [0][-1] )
        return (  evinfo + (tdc,)   , header, data)
    
if __name__ == '__main__':
    with open('../data/mamba/Run_Bias_Scan-B1-A-1061-13979.dat') as f:

        reader = mambareader(f)
        
        (evinfo,header,data) = reader.next_event()
        print evinfo
