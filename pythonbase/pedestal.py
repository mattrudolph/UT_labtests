#!/usr/bin/env python
"""
pedestal.py

Computes pedestals for either alibava data file or mamba file.  Also performs event by event commonmode subtraction on the pedestal subtracted data
"""

import alibavareader
import mambareader

import math

import sys
def commonmode( pedsub, startthreshold = 200, nit = 3, blocksize=32, channelmask=None ):
    """Common mode subtracts the given pedestal subtracted data.  Can adjust threshold to consider a value as a signal to be removed, the number of iterations, and how many channels to average together, returns ( [channel values], [block shifts])"""
    #break up into blocksize blocks
    sublists = [ pedsub[i:i+blocksize] for i in range(0,len(pedsub),blocksize) ]

    #do the CM subtraction for each block:

    multiplier = 3 #how many sigma around the mean to keep in the next iteration
    totshifts = []
    for iblock,channels in enumerate(sublists):
        #calculate the total shift for this block
        totshift = 0.0

        #pull out the subsection of the mask
        if channelmask:
            submask = channelmask[ iblock*blocksize : (iblock+1)*blocksize ]
        else:
            submask = [True,]*blocksize

        #if we don't have 2 channels on, skip this
        if sum( m for m in submask ) < 2:
            totshifts.append(totshift)
            continue
            
        threshold = startthreshold
        for it in range(nit):
            cm = 0.0
            cm2 = 0.0
            nused = 0.0
            for ichan,val in enumerate(channels):
                if submask[ichan] and abs(val) < threshold:
                    cm += val
                    cm2 += val*val
                    nused += 1
            if(nused < 2):
                if( it == 0): #If this is the first pass try again with a higher threshold, this only happens when every element of channels is large, so will find some values to use eventually (although the result will likely be garbage)

                    channels[:],tempshift = commonmode( channels, threshold*2, nit, blocksize, channelmask )    
                    totshift += tempshift[0]
                break #Don't iterate anymore

            totshift += -cm/nused
            channels[:] = [ c - cm/nused for c in channels ]
            if( nused == 1.0):
                print nused 
            threshold = multiplier*math.sqrt( (cm2 - (cm**2)/nused)/(nused-1)   )

        totshifts.append(totshift)

    retlist = []
    for sub in sublists:
        retlist += sub

    return retlist, totshifts
        
def computepedestal(filereader, channelmask = None, maxevents = -1):
    """Compute pedestal and common-mode-subtracted noise using the input data file"""


    #print "DEBUG: call compute pedstal"
    #print "Compute pedestal with channelmask =",channelmask
        
    nev = 0.0
    myped = []

    #First create the pedestal -- use the average since we are using a separate pedestal run so the rolling update doesn't mean much
    filereader.resetfile()
    ev = filereader.next_event()
    while ev is not None and (maxevents < 0 or nev < maxevents):
        nev += 1
        adcs = ev[-1]
        if not myped:
            myped = [0.0]*len(adcs)
            
        for i,raw in enumerate(adcs):
            myped[i] += raw
                
        ev = filereader.next_event()

    print "Processed", nev,"to calculate pedestal"
    myped[:] = [ p / nev for p in myped ]

    cmsnoise = []
    cmsnoise2 = []
    nevs = []
    
    # Need to run again to calculate the noise since needed the pedestal subtraction
    #print "Calculating noise after common-mode subtraction"
    filereader.resetfile()
        
    ev = filereader.next_event()
    nev=0
    while ev is not None and (maxevents < 0 or nev < maxevents):
        nev+=1
        cm = 0.0
        adcs = ev[-1]

        if not cmsnoise:
            cmsnoise = [0.0]*len(adcs)
            cmsnoise2 = [0.0]*len(adcs)
            nevs = [0]*len(adcs)
            
        pedsub = [adcs[i] - myped[i] for i in range(len(adcs)) ]
        cmsub,_ = commonmode( pedsub, channelmask = channelmask )

        for i,cm in enumerate(cmsub):
            #if abs(cm) < 100:
            nevs[i] += 1
            cmsnoise[i] +=  cm
            cmsnoise2[i] += cm*cm
                
                
        ev = filereader.next_event()

    for i,n in enumerate(cmsnoise):
        cmsnoise[i] = math.sqrt( (cmsnoise2[i] -  n**2/nevs[i])/(nevs[i]-1) )
        if cmsnoise[i]==0:
            cmsnoise[i] = 1 #noise floor -- reasonable? why are some getting 0 anyway

    #print "Returning pedestal and noise"
    return myped, cmsnoise

if __name__ == '__main__':
    indir = 'data/test/LaserScan/0047/'


    import sys
    sys.argv.append( '-b' )

    ped,noise = computepedestal(indir)
    
    import os
    plotdir = indir + 'pedplots'
    if not os.path.exists(plotdir):
        os.makedirs(plotdir)
    
    import ROOT
    c = ROOT.TCanvas()
    myped = ROOT.TH1F("mypedestal","pedestal",128,-0.5,127.5)
    mynoise = ROOT.TH1F("mynoise","noise",128,-0.5,127.5)

    for i,n in enumerate(ped):
        if i==myped.GetNbinsX():
            break
        myped.SetBinContent(i+1, ped[i])
        mynoise.SetBinContent(i+1,noise[i])
    
    myped.SetLineColor(ROOT.kBlue)
    mynoise.SetLineColor(ROOT.kBlue)
    
    myped.Draw("HIST")
    c.SaveAs(plotdir+"/pedestal.png")

    
    mynoise.Draw("HIST")
    c.SaveAs(plotdir+"/noise.png")
    
