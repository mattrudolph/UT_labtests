#!/usr/bin/env python

"""
clusterizer.py

Collection of clustering related functions for use with sensor lab tests.  

clusterize(): method takes as input the common-mode subtracted signals and the noise, and return a list of clusters which are themselves just lists of channels.  It can be configured with the high threshold cut (in sigma_noise), the low threshold for adding additional channels, and the polarity of signals.

clusterpos(): calculates the cluster position (in channel space), given the list of channels in the cluster and the common-mode subtracted signals.  By default it simply takes the signal weighted average of the channel numbers.  Alternatively an external eta to position function can be provided
"""


def clusterize(cmsub, noise, high=5, low=3, polarity=-1, channelmask=None):

    sn = []
    clusters = []

    nchan = len(cmsub)
    i = 0
    while i < nchan:
        sn.append( polarity*cmsub[i]/noise[i] )
        if (not channelmask or channelmask[i]) and sn[i] > high:
            clusters.append( [i,] )
            #look left
            j=i-1
            while( j>=0 and (not channelmask or channelmask[j]) and sn[j] > low ):
                clusters[-1].append(j)
                j -= 1
            #look right -- remember we have to add to the list to do this
            i += 1
            if i == nchan:
                break
            sn.append(polarity*cmsub[i]/noise[i])
            while( (not channelmask or channelmask[i]) and sn[i] > low ):
                clusters[-1].append(i)
                i += 1
                if i == nchan:
                    break
                sn.append(polarity*cmsub[i]/noise[i])
        
        i += 1

    return clusters

def clusterpos( cluster, cmsub, etaToPos = None ):
    if len(cluster) == 1:
        return float(cluster[0])
    
    if len(cluster) == 2:
        totsig =float( cmsub[cluster[0]] + cmsub[cluster[1]])
        eta = cmsub[cluster[1]]/totsig
        if etaToPos is None:
            return cluster[0] + eta
        else:
            return cluster[0] + etaToPos(eta) #until i implement a correct calculation w/ eta distribution

    return sum([float(cmsub[c])*c for c in cluster])/sum([float(cmsub[c]) for c in cluster])
