#!/usr/bin/env python

"""
Basic analysis of a single file taken with the mamba board

To run, execute:
python mambabaseplots.py path/to/datafile path/to/pedestalfile

Defines mambabaseplotter class inheriting from mambaproc.  Creates the following histograms in the mambaproc framework:
hits: channel numbers with hits (signal > 5*noise)
sigs: ADC signals for hit channels

Additionally defines the following methods:
plot_pedestal: creates histograms and plots the computed pedestal and noise
end_file: Saves plots after looping all events in the file

The main script is used to run the plotter
"""


from pythonbase.mambaproc import mambaproc
import ROOT
import pythonbase.pedestal as pedestal

class mambabaseplotter(mambaproc):
    def __init__(self,datfile,pedfile):
        mambaproc.__init__(self,[datfile,],pedfile)

        #Keep track of what channels get hits, and what the signal size is
        self.hits = ROOT.TH1F("hits","hits",512,-0.5,511.5)
        self.sigs = ROOT.TH1F("sigs","sigs",500,0,1000)

    def proc_event(self,ev):

        data = ev[-1]
        pedsub =  [ data[i] - self.ped[i] for i in range(len(data))]
        cmsub,_ = pedestal.commonmode( pedsub )

        for c,v in enumerate(cmsub):
            ##For this example, just ignore polarity
            if abs(v) > 5*self.noise[c]:
                self.hits.Fill(c)
                self.sigs.Fill(abs(v))

    def end_file(self):
        ##Plot the pedestal and noise
        self.plot_pedestal()

        self.canv.cd()

        #Save my hit and signal plot
        self.hits.Draw()
        self.saveplot("hits")
        self.sigs.Draw()
        self.saveplot("sigs")
        

    
if __name__== '__main__':
    import sys

    if len(sys.argv) < 3:
        print "Pass the dat file and pedestal file"
        sys.exit()

    infile = sys.argv[1]
    pedfile = sys.argv[2]
    
    sys.argv.append('-b')

    plotter = mambabaseplotter(infile,pedfile)

    plotter.loop_files()
    plotter.end_file()
    
