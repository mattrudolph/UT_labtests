#!/usr/bin/env python

"""Produce plots for laser scan along boundary between two strips with particular focus on behavior in the embedded pitch adapter regions"""

from pythonbase.alibavareader import *

from pythonbase.aliproc import aliproc
import ROOT
import pythonbase.pedestal as pedestal

class stripscan(aliproc):
    def __init__(self,indir,channels, firstconnected=64, lastconnected=127):
        aliproc.__init__(self,indir)

        self.chan = channels
        self.firstconnected = firstconnected
        self.lastconnected = lastconnected
        
        self.canv = ROOT.TCanvas()

        self.hits = ROOT.TH1F("hits","hits",128,-0.5,127.5)
        self.sig = ROOT.TH1F("sig","sigs",512,-512,512)

        self.mask = None

        self.sigs = []
        self.grsvx = []
        for c in self.chan:
            self.sigs += [ROOT.TH1F("sigs%i" % c,"sigs",512,-512,512),]
            self.grsvx += [ROOT.TGraph(),]

        self.hitvx = ROOT.TGraph()
        self.sigvx = ROOT.TGraph()
        
        self.wronghits = ROOT.TH1F("wronghits","hits; Chan",64,63.5,127.5)
        self.wronghitscontrol = ROOT.TH1F("wronghitscontrol","hits; Chan",64,63.5,127.5)

        self.evenadc1 = ROOT.TH1F("evenadc1","adc even chan; ADC", 200, -100,100)
        self.oddadc1 = ROOT.TH1F("oddadc1","adc odd chan; ADC", 200, -100,100)
        self.evenadc2 = ROOT.TH1F("evenadc2","adc even chan; ADC", 200, -100,100)
        self.oddadc2 = ROOT.TH1F("oddadc2","adc odd chan; ADC", 200, -100,100)
        
    def begin_file(self,j):
        #this scan moves down in x from the edge
        self.dx = -(self.xfiles[j-1] - self.xfiles[0])
        self.traceregion1 = (self.dx > 500 and self.dx < 700)
        self.traceregion2 = (self.dx > 1300 and self.dx < 1500)
        self.controlregion1 = (self.dx > 200 and self.dx < 400)
        self.controlregion2 = (self.dx > 800 and self.dx < 1000)
        
        self.dy = self.yfiles[j-1] - self.yfiles[0]
        
        self.hits.Reset()
        self.sig.Reset()
        for h in self.sigs:
            h.Reset()
        
    def proc_event(self,ev):
        data = ev[-1]
        pedsub =  [ ev[i] - self.ped[i] for i in range(self.firstconnected,self.lastconnected+1)]
        cmsub,_ = pedestal.commonmode( pedsub )

        for i,c in enumerate(self.chan):
            val = cmsub[ c - firstconnected ]
            
            if val < -5*self.noise[c]:
                self.hits.Fill(c)
                self.sigs[i].Fill(val)
                self.sig.Fill(val)
        
        for i,v in enumerate(cmsub):
            c = i + firstconnected
            if c in self.chan:
                continue

            if self.mask is not None and c in self.mask:
                continue

            if c >= 64 and c < min(self.chan):
                if c % 2 == 0:
                    if self.traceregion1:
                        self.evenadc1.Fill(v)
                    elif self.traceregion2:
                        self.evenadc2.Fill(v)
                else:
                    if self.traceregion1:
                        self.oddadc1.Fill(v)
                    elif self.traceregion2:
                        self.oddadc2.Fill(v)
                    
                        
            if v < -5*self.noise[c]:
                self.hits.Fill(c)
                self.sig.Fill(v)

                if self.traceregion1 or self.traceregion2:
                    self.wronghits.Fill(c)
                elif self.controlregion1 or self.controlregion2:
                    self.wronghitscontrol.Fill(c)
                    

    def end_file(self,j):
        self.canv.cd()
        
        hitchan = self.hits.GetMaximumBin()
        self.hitvx.SetPoint(self.hitvx.GetN(), self.dx, self.hits.GetBinContent(hitchan))

        for i,g in enumerate(self.grsvx):
            g.SetPoint(g.GetN(), self.dx, self.hits.GetBinContent( self.chan[i] ))
        
        self.hits.Draw()
        self.canv.SaveAs(self.plotdir+("hits_%04i.png" %j))

        totsig = 0
        for h in self.sigs:
            if h.GetEntries() > 0:
                totsig -= h.GetBinCenter(h.GetMaximumBin())

        self.sigvx.SetPoint(self.sigvx.GetN(), self.dx, totsig)

        self.sig.Draw()
        self.canv.SaveAs(self.plotdir+("sigs_%04i.png" %j))


    def sum_plots(self):
        self.canv.cd()
        lg = ROOT.TLegend(0.7,0.25,0.9,0.45)
        self.hitvx.SetMarkerStyle(20)

        self.hitvx.Draw("AP")
        colors = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen + 3]
        for i,g in enumerate(self.grsvx):
            g.SetMarkerStyle(20)
            g.SetMarkerColor(colors[i])
            lg.AddEntry(g,"Hit Chan. %i" % self.chan[i],"P")
            g.Draw("SAME P")

        self.hitvx.GetXaxis().SetTitle("x [#mum]")
        self.hitvx.GetYaxis().SetTitle("Hits")
        lg.AddEntry(self.hitvx, "Either", "P")
        lg.Draw()
        self.canv.SaveAs(self.plotdir + "hitvx.pdf" )

        self.sigvx.SetMarkerStyle(20)
        self.sigvx.Draw("AP")
        
        self.sigvx.GetXaxis().SetTitle("x [#mum]")
        if len(self.chan) > 0:
            ytitle =  "ADC(%i)" % self.chan[0]
        else:
            ytitle = ""
        for ic in range(1,len(self.chan)):
            ytitle += " + ADC(%i)" % self.chan[ic]
            
        self.sigvx.GetYaxis().SetTitle(ytitle)

        self.canv.SaveAs(self.plotdir + "sigvx.pdf" )

        self.wronghits.SetLineColor(ROOT.kRed)
        self.wronghitscontrol.SetLineColor(ROOT.kBlue)        
        leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
        leg.AddEntry(self.wronghits,"Trace region","L")
        leg.AddEntry(self.wronghitscontrol,"Control region","L")
        self.wronghits.Draw("HIST")
        self.wronghitscontrol.Draw("HIST SAME")
        leg.Draw()
        self.canv.SaveAs(self.plotdir+"wronghits.pdf")

        self.evenadc1.SetLineColor(ROOT.kBlue)
        self.evenadc2.SetLineColor(ROOT.kBlue)
        self.oddadc1.SetLineColor(ROOT.kRed)
        self.oddadc2.SetLineColor(ROOT.kRed)

        leg.Clear()
        leg.AddEntry(self.evenadc1,"Even channels","L")
        leg.AddEntry(self.oddadc1,"Odd channels","L")
        self.evenadc1.Draw()
        self.oddadc1.Draw("SAME")
        txt = ROOT.TPaveText(0.25,0.7,0.45,0.8,"NDC")
        txt.AddText( "#Delta<ADC> = %f" % (self.oddadc1.GetMean() - self.evenadc1.GetMean()))
        txt.Draw()
        leg.Draw()
        self.canv.SaveAs(self.plotdir+"evenodd1.png")
        
        self.evenadc2.Draw()
        self.oddadc2.Draw("SAME")
        txt.Clear()
        txt.AddText( "#Delta<ADC> = %f" % (self.oddadc2.GetMean() - self.evenadc2.GetMean()))
        txt.Draw()
        leg.Draw()
        self.canv.SaveAs(self.plotdir+"evenodd2.png")
        
        
                
if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        print "Pass the directory to process as the first argument"
        sys.exit()

    indir = sys.argv[1]


    ROOT.gROOT.Macro("~/root/rootlogon.C")

    #These are the channels we expect the scan to hit
    firstconnected = 64
    lastconnected = 127
    chan = [83,82]
    

    sys.argv.append( '-b' )

    plotter = stripscan(indir,chan)
    plotter.mask = [84,85]

    plotter.loop_files()
    plotter.sum_plots()

    
