if __name__== '__main__':
    import sys
    import argparse

    parser=argparse.ArgumentParser(description='Compare Mamba pedestal runs')
    parser.add_argument('--startDelay',type=int,default=1)
    parser.add_argument('--stopADC',type=int,default=37)
    parser.add_argument('infiles',type=str,nargs='+')

    args = parser.parse_args()


    def findrunno( f ):
        try: ##first try old style ...-runno-keplerno.dat
            num = int(f.split('-')[-2])
        except ValueError: ##now try new style
            try:
                num = int( f.split('-')[-1].split('.')[0] )
            except ValueError:
                ##don't know what's up with your file
                num = 0
        return num
    
    import re

    filelist = sorted([ f for f in args.infiles if re.match(".*Pedestal.*\.dat",f) is not None],key=findrunno)
    
    if len(filelist) == 0:
        print "Can't find any input files of the form '*Pedestal*.dat'"
        sys.exit()
        
    import os

    splitpath = os.path.normpath(filelist[0]).split('/')
    if len(splitpath) > 2:
        last2 = os.path.normpath(filelist[0]).split('/')[-3:-1]
        if len(filelist) > 1:
            plotdir = os.path.join("plots/",  last2[0], last2[1], "Ped" + str(findrunno(filelist[0])) + "-" + str(findrunno(filelist[-1])))
        else:
            plotdir = os.path.join("plots/",  last2[0], last2[1], "Ped" + str(findrunno(filelist[0])))

    else:
        ## fallback to just plots/
        if len(filelist) > 1:
            plotdir = os.path.join("plots/", "Ped" + str(findrunno(filelist[0])) + "-" + str(findrunno(filelist[-1])))
        else:
            plotdir = os.path.join("plots/",  "Ped" + str(findrunno(filelist[0])))
            
    print "Save plots to",plotdir
    
    if not os.path.exists(plotdir):
        os.makedirs(plotdir)
    
    sys.argv.append('-b')
    
    import ROOT
    if os.path.exists("~/rootlogon.C"):
        ROOT.gROOT.Macro("~/rootlogon.C")    

    maxevents = 10000
    
    #check header
    from mamba_analysis import mambaanalysis
    plotter = mambaanalysis( filelist[:1], plotdir= plotdir+"/", maxevents=maxevents,startDelay=args.startDelay, stopADC=args.stopADC )
    #plotter.addinterestingchannels(range(128,256)+range(384,512))
    plotter.loop_files()
    plotter.plot_pedestal()
    plotter.plot_headers(plotall=True)
    plotter.plot_cmshifts()
    plotter.plot_interesting()
    
    if len(filelist) > 1:
        
        from pythonbase.pedestal import *
        hpeds = []
        hnoises = []
        for j,f in enumerate(filelist):
            newpedestal,newnoise = computepedestal( f, ismamba=True, maxevents=maxevents)
            hpeds += [ ROOT.TH1F("newpedestal_{}".format(j),"pedestal;Channel;Pedestal [ADC]",512,-0.5,511.5)]
            hnoises += [ ROOT.TH1F("newnoise_{}".format(j),"noise;Channel;Noise [ADC]",512,-0.5,511.5)]
            for i,n in enumerate(newpedestal):
                hpeds[-1].SetBinContent(i+1, newpedestal[i])
                hnoises[-1].SetBinContent(i+1,newnoise[i])
            

        c = ROOT.TCanvas()

        hdiff = ROOT.TH1F("diff","ped diff",120,-3,3)
    
        hpeds[1].Add(hpeds[0],-1.0)
        for j in range(1,hpeds[1].GetNbinsX()+1):
            hdiff.Fill(hpeds[1].GetBinContent(j))
        hpeds[1].Draw("HIST")
        hpeds[1].GetYaxis().SetTitle("#Delta(pedestal) [ADC]")
        hpeds[1].GetYaxis().SetRangeUser(-50,50)

        for i,h in enumerate(hpeds[2:]):
            h.Add(hpeds[0],-1.0)

            for j in range(1,h.GetNbinsX()+1):
                hdiff.Fill(h.GetBinContent(j))
        
            if i <5:
                h.SetLineColor( ROOT.myColSet1[i] )
            else:
                h.SetLineColor( ROOT.myColSet2[i-5] )
            h.Draw("SAME HIST")

        c.SaveAs( plotdir + '/pedestal_comp.pdf')

        hdiff.Draw()
        hdiff.GetXaxis().SetTitle("#Delta(pedestal) [ADC]")
        c.SaveAs( plotdir + '/pedestal_diff.pdf')
