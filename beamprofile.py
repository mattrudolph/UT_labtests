#!/usr/bin/env python


"""beamprofile.py: Analyze a set of aliscan runs that trace out the transition from one strip to the next; use to determine the width of the laser beam"""

from pythonbase.aliproc import aliproc
import ROOT
import pythonbase.pedestal as pedestal

class beamprofile(aliproc):
    def __init__(self,indir, channels):
        self.hfilename="beamprofile_histograms.root"
        aliproc.__init__(self,indir)

        self.chan = channels

        self.canv = ROOT.TCanvas()

        self.hits = ROOT.TH1F("hits","hits",256,-0.5,255.5)
        self.sig = ROOT.TH1F("sig","sigs",512,-512,512)

        self.sigs = []
        self.cms = []
        self.grs = []
        for c in self.chan:
            self.sigs += [ROOT.TH1F("sigs%i" % c,"sigs",512,-512,512),]
            self.cms += [ROOT.TH1F("cms%i" % c,"sigs",512,-512,512),]
            self.grs += [ROOT.TGraph(),]


        self.myfunc = ROOT.TF1("gaus","gaus")
        self.myfunc.SetParameter(0,4000)
        self.myfunc.SetParameter(1,-200)
        self.myfunc.SetParameter(2,5)


    def begin_file(self,j):
        self.hits.Reset()
        self.sig.Reset()
        for h in self.sigs:
            h.Reset()

    def proc_event(self,ev):
        for i,c in enumerate(self.chan):
            
            cms,_ = pedestal.commonmode([ ev[2][k] - self.ped[k] for k in range( c - (c % 32), c - (c % 32) + 32 )])
                    
            val = cms[ c % 32 ]
                    
            self.cms[i].Fill(val)

            if val*self.polarity > 5*self.noise[c]:
                self.hits.Fill( c )
                self.sigs[i].Fill(val)
                self.sig.Fill(val)

    def end_file(self,j):
        self.canv.cd()
        self.sig.Draw()
        #self.canv.SaveAs(self.plotdir+("/sigs_%02i.png" % j))

        dy = self.yfiles[j]# - self.yfiles[0]

        for i,h in enumerate(self.sigs):
            h.Draw()
            #self.myfunc.SetParameter(1, h.GetBinCenter( h.GetMaximumBin() ) )
            if(h.GetEntries() > 10 ):
                s = self.polarity*h.GetBinCenter( h.GetMaximumBin() )
                self.grs[i].SetPoint(self.grs[i].GetN(), dy, s)
                
            else:
                self.grs[i].SetPoint(self.grs[i].GetN(), dy, 0)

            #self.canv.SaveAs(self.plotdir+("/sigs_%02i_%i.png" % (j,i) ) )

    def sum_plots(self):
        self.canv.cd()
        colors = [ 1, ROOT.kBlue, ROOT.kRed, ROOT.kGreen + 3]

        leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

        xmin = 0
        xmax = 1 
        x = ROOT.Double()
        y = ROOT.Double()       
        for i,g in enumerate(self.grs):
            g.SetMarkerStyle(20)
            g.SetLineColor(colors[i])
            g.SetMarkerColor(colors[i])
            self.outfile.cd()
            g.Write("grpeak_chan{}".format(self.chan[i]))

            leg.AddEntry( g, "Strip %i" % self.chan[i], "P")
        
            if i==0:
                g.Draw("AP")
                g.GetXaxis().SetTitle("y [#mum]")
                g.GetYaxis().SetTitle("Peak ADC")
                g.GetYaxis().SetRangeUser(0,200)
                xmin = g.GetHistogram().GetXaxis().GetXmin()
                xmax = g.GetHistogram().GetXaxis().GetXmax()
                print xmin,xmax
            else:
                g.Draw("SAME P")
                if i==1:
                    m = 0.0
                    for j in range( g.GetN() ):
                        g.GetPoint(j, x,y)
                        if y > m:
                            m = float(y)
                    self.canv.lin1=ROOT.TLine(xmin, m, xmax, m)
                    self.canv.lin1.Draw()
                    self.canv.txt1 = ROOT.TPaveText(0.2,0.8,0.4,0.85,"NDC")
                    self.canv.txt1.SetTextColor(colors[i])
                    self.canv.txt1.SetFillColor(0)
                    self.canv.txt1.SetBorderSize(0)
                    self.canv.txt1.AddText("ADC_{{max}} = {:.0f}".format(m))
                    self.canv.txt1.Draw()
                elif i==2:
                    m = 0.0
                    for j in range( g.GetN() ):
                        g.GetPoint(j, x,y)
                        if y > m:
                            m = float(y)
                    self.canv.lin2=ROOT.TLine(xmin, m, xmax, m)
                    self.canv.lin2.Draw()
                    self.canv.txt2 = ROOT.TPaveText(0.2,0.7,0.4,0.75,"NDC")
                    self.canv.txt2.SetTextColor(colors[i])
                    self.canv.txt2.SetFillColor(0)
                    self.canv.txt2.SetBorderSize(0)
                    self.canv.txt2.AddText("ADC_{{max}} = {:.0f}".format(m))
                    self.canv.txt2.Draw()
                    
        
                
        leg.Draw()

        self.saveplot("peak_pos")

        grprof = ROOT.TGraph()
        midguess = 0
        guessdist = 10000
        u = ROOT.Double()
        v = ROOT.Double()
        for i in range(self.grs[0].GetN()):
            self.grs[1].GetPoint( i, x,y)
            self.grs[2].GetPoint( i, u,v)

        #print "i=",i,": (x,y)=", x, ",",y,"; (u,v)=",u,",",v
            if( y > 80 and v > 80):
                if abs(y-v) < guessdist:
                    midguess = i
                    guessdist = abs(y-v)

        mx = self.xfiles[midguess]
        my = self.yfiles[midguess]

        print "Guess crossover at", midguess, " step pos x=",mx,", y=",my 
                
        for i in range( midguess - 15, midguess + 15 ):
            self.grs[1].GetPoint( i-1, x,y)
            self.grs[1].GetPoint( i+1, u,v)
            d0 = -( v - y)/2
        
            self.grs[2].GetPoint( i-1, x,y)
            self.grs[2].GetPoint( i+1, u,v)

            d1 = ( v - y)/2

            self.grs[1].GetPoint( i, x,y)
        
            grprof.SetPoint( grprof.GetN(), x, (d0+d1)/2)

        grprof.SetMarkerStyle(20)

        grprof.Draw("AP")
        grprof.GetXaxis().SetTitle("y [#mum]")
        grprof.GetYaxis().SetTitle("dADC/dy")
        grprof.SetName("beamprofile")
        self.saveplot("beamprofile",grprof)

        for i,h in enumerate(self.cms):
            h.SetLineColor(colors[i])
            self.outfile.cd()
            h.Write()
            if i==0:
                h.Draw()
                h.GetXaxis().SetRangeUser(-150,150)
                h.GetXaxis().SetTitle("CMS ADC")
            else:
                h.Draw("SAME")
        leg.Draw()
        self.saveplot("allcms")

if __name__ == '__main__':
    import sys

    if len(sys.argv) < 2:
        print "Pass the directory to process as the first argument"
        sys.exit()

    indir = sys.argv[1]

    sys.argv.append('-b')
    
    ROOT.gROOT.Macro("~/rootlogon.C")

    ##Specify input directory and a list of channels to monitor
    plotter = beamprofile(indir, [49,50,51,52])

    # ##specify files to loop over

    # runnums = range(287,332)
    # # #runnums = range(1390,1398)
    # flist = []
    # for run in runnums:
    #     for i,f in enumerate(plotter.datfiles):
    #         if str(run) in f:
    #             flist.append(i)

    # print flist
    # for i in flist:
    #     print plotter.datfiles[i]
    #     print plotter.yfiles[i]
    #plotter.loop_files(flist)

    plotter.loop_files()

    plotter.sum_plots()
