import ROOT

from mamba_analysis import mambaanalysis
from pythonbase.pedestal import *

class calibanalysis(mambaanalysis):
    def __init__(self,datfile,pedfile=None,plotdir=None,maskedchannels=None):
        mambaanalysis.__init__(self,datfile,pedfile,plotdir,maskedchannels)

        ### define new histograms here
        ## self.mynewhist = ROOT.TH1F(....)

    def loop_file(self,datfile):
        ##Calibration runs flip polarity on consecutive runs
        self.polarity *= -1.0
        mambaanalysis.loop_file(self,datfile)
        
    def proc_event(self,ev):
        #Call parent event processor
        mambaanalysis.proc_event(self,ev)

        # fill my new histograms here
        ## self.mynewhist.Fill( ... )
        
    def sum_plots(self):
        #Call the plot saving methods of parent analysis
        self.plot_pedestal()
        self.plot_cmshifts()
        self.plot_headers()
        self.plot_interesting()
        
        self.plot_hits()
                
        #make sure canvas is active
        self.canv.cd()

        #Save my new histograms here

        
if __name__== '__main__':
    import sys

    import argparse

    parser=argparse.ArgumentParser(description='Analyze Mamba RS runs')
    parser.add_argument('indir',type=str)
    args = parser.parse_args()
    
    indir = args.indir
    import os
    last3 = os.path.normpath(indir).split('/')[-3:]
    plotdir = os.path.join("plots/", last3[0], last3[1], last3[2] )
    
    print "plotdir=",plotdir
    
    if not os.path.exists(plotdir):
        os.makedirs(plotdir)
    
    
    sys.argv.append('-b')

    ROOT.gROOT.Macro("~/rootlogon.C")

    import re

    def findrunno( f ):
        return int(f.split('-')[-2])
    
    filelist = sorted([ os.path.join(indir,f) for f in os.listdir(indir) if re.match("Calibration.*\.dat",f) is not None],key=findrunno)

    #Because some calib scans are mislabeled
    if len(filelist) == 0:
        filelist = sorted([ os.path.join(indir,f) for f in os.listdir(indir) if re.match("Run_Angle_.*\.dat",f) is not None],key=findrunno)

    if len(filelist) == 0:
        print "Can't find files to run on in path", indir
        sys.exit()

    # print filelist

    #What file to use to calculate the pedestal for most channels
    pedestal_runno = 40

    ##Create the plotter
    plotter = calibanalysis(filelist,pedfile=filelist[pedestal_runno],plotdir=plotdir+'/')
###    plotter.plotdir = plotdir + '/'
    plotter.maxevents = 1000
    #plotter.addinterestingchannels( [0,128,256,384,511,186,251,283])

    
    # Recompute pedestal for channel + n*128 that were being calibrated in first file and the next channel over
    newpedestal, newnoise = computepedestal( filelist[pedestal_runno + 10 % 512], ismamba=True, maxevents=1000)

            
    plotter.loop_files()         
        
    plotter.sum_plots()
