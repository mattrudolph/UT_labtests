"""
Analysis of all files from multiple aliscan laser runs that go parallel across whole detector

To run, execute:
python aliscan_parallelanalysis.py path/to/directory path/to/nextdirectory ...
"""

from pythonbase.aliproc import aliproc
import ROOT
import pythonbase.pedestal as pedestal

class parallelplotter(aliproc):
    def __init__(self,chan,*indirs):
        aliproc.__init__(self,*indirs)

        self.chan = chan
        
        ##All ADC values checked strip
        self.sigs = ROOT.TH1F("sigs","sigs;ADC",512,-512,512)
        self.sigsplus = ROOT.TH1F("sigsplus","sigs;ADC",512,-512,512)
        self.sigsminus = ROOT.TH1F("sigsminus","sigs;ADC",512,-512,512)

        ##ADC values in one run for checked strip
        self.sig = ROOT.TH1F("sigvchan{}".format(chan),"adc signal for channel", 100,0,200)
        self.sigp = ROOT.TH1F("sigvchan{}".format(chan+1),"adc signal for channel plus 1", 100,0,200)
        self.sigm = ROOT.TH1F("sigvchan{}".format(chan-1),"adc signal for channel minus 1", 100,0,200)


        self.grxpeak = ROOT.TGraph()
        self.grxpeak.SetName("grxpeak")
        self.grxmean = ROOT.TGraph()
        self.grxmean.SetName("grxmean")
        
        self.grypeak = ROOT.TGraph()
        self.grypeak.SetName("grypeak")
        self.grymean = ROOT.TGraph()
        self.grymean.SetName("grymean")
        
    def begin_file(self, j):
        self.sig.Reset()
        self.sigp.Reset()
        self.sigm.Reset()

    def proc_event(self, ev):
        
        data = ev[-1]
        pedsub =  [ data[i] - self.ped[i] for i in range(len(data))]
        cmsub,_ = pedestal.commonmode( pedsub )

        v = cmsub[self.chan]
        if v > 5*self.noise[self.chan]:
            self.sigs.Fill(v)
            self.sig.Fill(v)

        v = cmsub[self.chan+1]
        if v > 5*self.noise[self.chan+1]:
            self.sigsplus.Fill(v)
            self.sigp.Fill(v)

        v = cmsub[self.chan-1]
        if v > 5*self.noise[self.chan-1]:
            self.sigsminus.Fill(v)
            self.sigm.Fill(v)
            
    def end_file(self, j):

        mean = self.sig.GetMean()
        peak = self.sig.GetBinCenter( self.sig.GetMaximumBin())

        self.grxmean.SetPoint( self.grxmean.GetN(), self.xfiles[j], mean )
        self.grxpeak.SetPoint( self.grxpeak.GetN(), self.xfiles[j], peak )
        
        self.grymean.SetPoint( self.grymean.GetN(), self.yfiles[j], mean )
        self.grypeak.SetPoint( self.grypeak.GetN(), self.yfiles[j], peak )



    def sum_plot(self):
        self.canv.cd()

        for h in [self.sigs, self.sigsplus, self.sigsminus]:
            h.Draw()
            self.saveplot(h.GetName(),h)

        self.canv.SetRightMargin(0.16)
        
        self.grxmean.Draw("AP")
        self.grxmean.GetXaxis().SetTitle("x [#mum]")
        self.grxmean.GetYaxis().SetTitle("Mean ADC")
        self.saveplot("xmean",self.grxmean)

        self.grxpeak.Draw("AP")
        self.grxpeak.GetXaxis().SetTitle("x [#mum]")
        self.grxpeak.GetYaxis().SetTitle("Peak ADC")
        self.saveplot("xpeak",self.grxpeak)
        
        self.grymean.Draw("AP")
        self.grymean.GetXaxis().SetTitle("y [#mum]")
        self.grymean.GetYaxis().SetTitle("Mean ADC")
        self.saveplot("ymean",self.grymean)

        self.grypeak.Draw("AP")
        self.grypeak.GetXaxis().SetTitle("y [#mum]")
        self.grypeak.GetYaxis().SetTitle("Peak ADC")
        self.saveplot("ypeak",self.grypeak)



if __name__== '__main__':
    import sys

    if len(sys.argv) < 2:
        print "Pass a directory to process as the first argument"
        sys.exit()

    indirs = sys.argv[1:]
    
    ##put ROOT in batch mode for no graphics
    sys.argv.append('-b')


    ROOT.gROOT.Macro("~/rootlogon.C")
    
    plotter = parallelplotter(50,*indirs)
    plotter.plot_pedestal()

    plotter.loop_files()
    
    plotter.sum_plot()
