
"""mamba_rsanalysis.py: Analyze the output from alibava_rstest.py for the directory specified on command line.  Uses aliproc as a base for the pedestal and noise calculation, but modifies the loop_file and proc_event methods to include the tdc time."""

from mamba_analysis import mambaanalysis
import ROOT
if __name__== '__main__':
    import sys

    import argparse

    parser=argparse.ArgumentParser(description='Analyze Mamba RS runs')
    parser.add_argument('infiles',type=str,nargs='+')
    parser.add_argument('-p',type=str)
    args = parser.parse_args()

    pedfile = args.p

    print "Input files=",args.infiles
    print "Pedestal file=",pedfile
    
    sys.argv.append('-b')

    ROOT.gROOT.Macro("~/rootlogon.C")

    def findrunno( f ):
        return f.split('-')[-2]

    import os
    last3 = os.path.normpath(args.infiles[0]).split('/')[-4:-1]
    plotdir = os.path.join("plots/", last3[0], last3[1], last3[2], findrunno(args.infiles[0]) ) + '/'
    print "plotdir=",plotdir

    
    #bondedchans = range(149,149+64) + range(409,409+64)
    #unbondedchans = [c for c in range(512) if c not in bondedchans ]

    unbondedchans = range(0,128) + range(256,384)
    #print "Bonded channels=",bondedchans
    #print "Unbonded channels=", unbondedchans

    ##these channels have crazy adc distributions not fixed by CM sub
    #badchans = [ 159, 169, 170, 248, 249 ]
    blocknums = [4,5,7,14,15]
    badchans = []
    for bn in blocknums:
        badchans += range(bn*32,(bn+1)*32)


    mask = unbondedchans + badchans
    print mask
    ##Bonded channels for minihybrid
    

    plotter = mambaanalysis(args.infiles,pedfile,plotdir)
    #plotter.maskchannels( [96,241,242,243,254,255,471]) #first and last connected channel
    
    # #investigate ADCs for channels producing strange hits
    #plotter.addinterestingchannels( [31,32,180, 200,400,410,440])
    #plotter.addinterestingchannels( [96, 242, 255, 471] )
    
    plotter.noise_limit=100
    plotter.flag_channels()

    
    plotter.loop_files()

    plotter.sum_plots()
