"""
Analysis of all files from multiple aliscan laser runs that go perpendicular across whole detector

To run, execute:
python aliscan_perpanalysis.py path/to/directory path/to/nextdirectory ...
"""

from pythonbase.aliproc import aliproc
import ROOT
import pythonbase.pedestal as pedestal

class perpplotter(aliproc):
    def __init__(self,*indirs):
        aliproc.__init__(self,*indirs)

        ##All strips numbers with hits 5x noise
        self.hitsall = ROOT.TH1F("hitsall","hits;Channel;Total hits",256,-0.5,255.5)
        
        ##All ADC values for hit strips
        self.sigsall = ROOT.TH1F("sigsall","sigs;ADC",512,-512,512)

        ##Max peak ADC of any run for each channel
        self.peakadc = ROOT.TH1F("peakadc","peakadc",256,-0.5,255.5)
        self.peakadc.GetXaxis().SetTitle("Channel")
        self.peakadc.GetYaxis().SetTitle("Max peak adc")
        
        self.peakadc_overn = ROOT.TH1F("peakadc_overn","peak adc over noise",256,-0.5,255.5)
        self.peakadc_overn.GetXaxis().SetTitle("Channel")
        self.peakadc_overn.GetYaxis().SetTitle("Max peak adc / noise")

        ##Max mean ADC of any run for each channel
        self.meanadc = ROOT.TH1F("meanadc","meanadc",256,-0.5,255.5)
        self.meanadc.GetXaxis().SetTitle("Channel")
        self.meanadc.GetYaxis().SetTitle("Max mean adc")
        self.meanadc_overn = ROOT.TH1F("meanadc_overn","mean adc over noise",256,-0.5,255.5)
        self.meanadc_overn.GetXaxis().SetTitle("Channel")
        self.meanadc_overn.GetYaxis().SetTitle("Max mean adc / noise")

        ##Highest mean ADC per run
        self.hichan = ROOT.TGraph()
        self.hichan.SetName("hichan")
        
        self.sigvchan = []
        for c in range(256):
            self.sigvchan.append(ROOT.TH1F("sigvchan{}".format(c),"adc signal v channel", 20,0,200))
        
    def begin_file(self, j):
        for h in self.sigvchan:
            h.Reset()

    def proc_event(self,ev):
        headers = ev[-2]
        for i,h in enumerate(headers):
            self.headers.Fill( i, h )
            self.headerdists[ i%16].Fill(h)

        data = ev[-1]
        pedsub =  [ data[i] - self.ped[i] for i in range(len(data))]
        cmsub,_ = pedestal.commonmode( pedsub )

        for c,v in enumerate(cmsub):
            if self.polarity*v > 5*self.noise[c]:
                self.hitsall.Fill(c)
                self.sigsall.Fill(v)

                self.sigvchan[c].Fill(self.polarity*v)

    def end_file(self,j):
        ##find the channel with the highest mean adc for this run
        himean = 0
        hichan = -1
        
        ##Find the peak ADC for each channel, and check if we should update the max peak
        for c,h in enumerate(self.sigvchan):

            if h.GetEntries() < 100:
                continue
            
            peak = h.GetBinCenter( h.GetMaximumBin() )
            mean = h.GetMean()

            if mean > himean:
                himean = mean
                hichan = c
                
            if peak > self.peakadc.GetBinContent( c+1 ):
                self.peakadc.SetBinContent( c+1, peak )
                self.peakadc_overn.SetBinContent( c+1, peak/ self.noise[c] )
            if mean > self.meanadc.GetBinContent( c+1 ):
                self.meanadc.SetBinContent( c+1, mean )
                self.meanadc_overn.SetBinContent( c+1, mean/ self.noise[c] )

        if hichan != -1:
            self.hichan.SetPoint( self.hichan.GetN(), self.yfiles[j], hichan )

    def sum_plot(self):
        self.canv.cd()
        for h in [self.hitsall,self.sigsall,self.peakadc,self.peakadc_overn,self.meanadc,self.meanadc_overn]:
            h.Draw()
            self.saveplot(h.GetName(),h)

        self.canv.SetRightMargin(0.16)
        self.hichan.Draw("AP")
        self.hichan.GetXaxis().SetTitle("y [#mum]")
        self.hichan.GetYaxis().SetTitle("Highest mean ADC channel")
        self.saveplot("hichan",self.hichan)
    
        
if __name__== '__main__':
    import sys

    if len(sys.argv) < 2:
        print "Pass a directory to process as the first argument"
        sys.exit()

    indirs = sys.argv[1:]
    
    ##put ROOT in batch mode for no graphics
    sys.argv.append('-b')


    ROOT.gROOT.Macro("~/rootlogon.C")
    
    plotter = perpplotter(*indirs)
    plotter.plot_pedestal()

    plotter.loop_files()
    
    plotter.sum_plot()
    plotter.plot_headers()
