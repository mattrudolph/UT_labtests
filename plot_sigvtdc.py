import ROOT
ROOT.gROOT.Macro('~/rootlogon.C')

ROOT.gSystem.Load('libRooFit')

mean = 200
sigma = 100

adc = ROOT.RooRealVar("adc","adc",0.0,1000.0)
adc.setRange("myrange",100,1000);



#        // Construct gauss(t,mg,sg)
mg =ROOT.RooRealVar("mg","mg",0) 
sg =ROOT.RooRealVar("sg","sg",sigma,0.1*sigma,5.*sigma) 
gauss=ROOT.RooGaussian("gauss","gauss",adc,mg,sg) 

#         // Construct landau(t,ml,sl) ;
ml=ROOT.RooRealVar("ml","mean landau",mean,0,400) 
sl=ROOT.RooRealVar("sl","sigma landau",0.04,0.,100.0) 
landau=ROOT.RooLandau("lx","lx",adc,ml,sl)

adc.setBins(5000,"cache")

#// Construct landau (x) gauss
lxg = ROOT.RooFFTConvPdf("lxg","landau (X) gauss",adc,landau,gauss)


f4 = ROOT.TFile("data/pipelinescan/Mamba/Custom-B1-A-20-0_plots/histograms.root")
f5 = ROOT.TFile("data/pipelinescan/Mamba/Custom-B1-A-25-0_plots/histograms.root")
f3 = ROOT.TFile("data/pipelinescan/Mamba/Custom-B1-A-30-0_plots/histograms.root")

hmaxvals = ROOT.TH1F("maxvals","maximum values",30,-1.25,73.75)
hmaxvals.Sumw2()

offset = 0

canv = ROOT.TCanvas()
for f in [f5,f4,f3]:
    h2d = f.Get("fullsigvtdc")

    for b in range(1,h2d.GetNbinsX()+1):
        h2d.GetXaxis().SetRange(b,b)
        h2d.GetYaxis().SetRange(11,100)
        h1d = h2d.ProjectionY()
        rh = ROOT.RooDataHist("rh","rh",ROOT.RooArgList(adc),h1d)
        lxgnew = lxg.Clone()
        lxgnew.fitTo(rh,ROOT.RooFit.Range("myrange"))
        
        #estimate relative error on peak position
        relerr = ml.getError()/ml.getVal()
        
        frame = adc.frame()
    
        rh.plotOn(frame)
        lxg.plotOn(frame)
    

        frame.Draw()
    
        canv.SaveAs("plots/adc_{}.pdf".format(b+offset))


        
        func = lxg.asTF( ROOT.RooArgList( adc) )
        x=ROOT.Double()
        y=ROOT.Double()
        func.GetRange(x,y)
        print x,y
        func.SetRange(0,1000)
        hmaxvals.SetBinContent(b+offset,func.GetMaximumX())
        hmaxvals.SetBinError(b+offset,relerr*func.GetMaximumX())
        
    offset += 10

hmaxvals.GetXaxis().SetTitle("ns")
hmaxvals.GetYaxis().SetTitle("Peak ADC")
hmaxvals.Draw()
hmaxvals.GetXaxis().SetRange(8,24)

height = 1.2*hmaxvals.GetBinContent(hmaxvals.GetMaximumBin())

hmaxvals.GetYaxis().SetRangeUser(0, height)
lin1 = ROOT.TLine( 23.75, 0, 23.75, height)
lin2 = ROOT.TLine( 48.75, 0, 48.75, height)
lin1.Draw()
lin2.Draw()
canv.SaveAs("plots/sigvtdc.pdf")

# tdc = ROOT.RooRealVar("tdc","tdc",0.0,30.0)
# tdc.setRange("tdcrange",9.5,19.5)
# th = ROOT.RooDataHist("th","th",ROOT.RooArgList(tdc),hmaxvals)

# sg.setVal(1)
# tgauss=ROOT.RooGaussian("gauss","gauss",tdc,mg,sg) 

# #         // Construct landau(t,ml,sl) ;
# ml=ROOT.RooRealVar("ml","mean landau",mean,0,400) 
# sl=ROOT.RooRealVar("sl","sigma landau",0.04,0.,100.0)
# ml.setVal(10)
# sl.setVal(0.1)
# tlandau=ROOT.RooLandau("lx","lx",tdc,ml,sl)
# tdc.setBins(5000,"cache")

# #// Construct landau (x) gauss
# tlxg = ROOT.RooFFTConvPdf("lxg","landau (X) gauss",tdc,tlandau,tgauss)

# tlxg.fitTo(th,ROOT.RooFit.Range("tdcrange"))

# tframe = tdc.frame()

# th.plotOn(tframe)
# tlxg.plotOn(tframe)
# tframe.Draw()
# canv.SaveAs("plots/sigvtdc_fit.pdf")
