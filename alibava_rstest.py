#!/usr/bin/env python

"""alibava_rstest.py: run a single pedestal and radioactive source run with the Alibava DAQ, saving the output in a new directory organized by a user inputted name for the sensor under test"""

import sys
from alibavaSOAP import Alibava, Status
import SOAPpy

host = "localhost"
port = 10000

server = Alibava(host,port)

s = Status( server.getStatus() )
print s


sensname = raw_input("Enter sensor name: ")
outdir = "/home/alibava-user/data/"+sensname+"/RsRun/"

import os
if not os.path.exists(outdir):
    os.makedirs(outdir)

dirnums = [ int(f) for f in os.listdir(outdir) if os.path.isdir(outdir+f) ]

if dirnums:
    newnum = max(dirnums) + 1
else:
    newnum = 0
outdir = outdir + ("%04i"%newnum)
print "Place output in", outdir

os.makedirs(outdir)

server.setDataFile( outdir + "/pedestal.dat" )

r = server.startPedestalRun(2000)

s = Status( server.getStatus() )
print s

server.setDataFile( outdir + "/rs_data.dat" )
r = server.startSourceRun(10000,nsample=1)

s = Status( server.getStatus() )
print s
