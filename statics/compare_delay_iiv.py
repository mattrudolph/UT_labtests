
import ROOT
ROOT.gROOT.Macro("~/rootlogon.C")
import plot_iiv

c = ROOT.TCanvas()

f0 = "data/iiv_tests_aug26/fixrange_60hz_16samples.dat"
f1 = "data/iiv_tests_aug26/fixrange_60hz_16samples_75delay.dat"

g237_0, g2002_0 = plot_iiv.file2graphs(f0)
g237_1, g2002_1 = plot_iiv.file2graphs(f1)



        
g237_0.SetLineColor(ROOT.myColSet2[0])
g237_0.SetMarkerColor(ROOT.myColSet2[0])

g2002_0.SetLineColor(ROOT.myColSet2[0])
g2002_0.SetMarkerColor(ROOT.myColSet2[0])
g2002_0.SetMarkerStyle(21)

g237_1.SetLineColor(ROOT.myColSet2[1])
g237_1.SetMarkerColor(ROOT.myColSet2[1])

g2002_1.SetLineColor(ROOT.myColSet2[1])
g2002_1.SetMarkerColor(ROOT.myColSet2[1])
g2002_1.SetMarkerStyle(21)

g237_0.Draw("ALP")
g237_0.GetXaxis().SetTitle("Bias [V]")
g237_0.GetYaxis().SetTitle("Current [nA]")

g237_0.GetYaxis().SetRangeUser(-10,30)

g237_1.Draw("SAME LP")
g2002_0.Draw("SAME LP")
g2002_1.Draw("SAME LP")
c.SaveAs("plots/comp_delay.pdf")

g237_0.GetYaxis().SetRangeUser(0,2)

c.SaveAs("plots/comp_delay_zoom.pdf")
