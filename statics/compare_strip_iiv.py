
import ROOT
ROOT.gROOT.Macro("~/rootlogon.C")
import plot_iiv

c237 = ROOT.TCanvas()
c2002 = ROOT.TCanvas()


fs = ["data/SingleStripAug29/strip1.dat",
      "data/SingleStripAug29/strip2.dat",
      "data/SingleStripAug29/strip5.dat",
      "data/SingleStripAug29/strip10.dat",
      ]
ns = [1,2,5,10]
g237s = []
g2002s = []
leg237 = ROOT.TLegend(0.6,0.3,0.9,0.6)
leg2002 = ROOT.TLegend(0.6,0.6,0.9,0.9)

for i,fn in enumerate(fs):
    g237, g2002 = plot_iiv.file2graphs(fn)

    g237.SetLineColor(ROOT.myColSet2[i])
    g237.SetMarkerColor(ROOT.myColSet2[i])

    g2002.SetLineColor(ROOT.myColSet2[i])
    g2002.SetMarkerColor(ROOT.myColSet2[i])
    g2002.SetMarkerStyle(21)

    g237s.append(g237)
    g2002s.append(g2002)

    leg237.AddEntry( g237, "Strip {}".format(ns[i]), "LP" )
    leg2002.AddEntry( g2002, "Strip {}".format(ns[i]), "LP" )
    
    if i==0:
        c237.cd()
        g237.Draw("ALP")
        g237.GetXaxis().SetTitle("Bias [V]")
        g237.GetYaxis().SetTitle("Current [nA]")

        g237.GetYaxis().SetRangeUser(-10,30)

        
        c2002.cd()
        g2002.Draw("ALP")
        g2002.GetXaxis().SetTitle("Bias [V]")
        g2002.GetYaxis().SetTitle("Current [nA]")

        g2002.GetYaxis().SetRangeUser(0,2)

    else:
        c237.cd()
        g237.Draw("SAME LP")

        c2002.cd()
        g2002.Draw("SAME LP")


c237.cd()
leg237.Draw()

c237.SaveAs("plots/singstrip237.pdf")

c2002.cd()
leg2002.Draw()
c2002.SaveAs("plots/singstrip2002.pdf")
