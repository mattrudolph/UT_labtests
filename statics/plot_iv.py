

"""plot_iv.py
Stuff to parse and plot info from IV data files

File has a header that is something like:

C:\Documents and Settings\lhcb\Desktop\Sept2016\SingleStripTestAug26\default_after3hrwarmup.dat
8/26/2016
4:14:41 PM
#RH= 39.00
Temp (C)= 19.40
COMMENTS : 
========== DATA : V --  (Time_237  I_237)*Nb of counts/profile --  (Time_2002  I_2002)*Nb of counts/profile --  min max avg err I_237 --  min max avg err I_2002  ==========
Number of points : 21

Then each of the next lines is data.  At the end are more lines like :

08/29/2016 14:26:51.125
RH= 36.10
Temp (C)= 18.70
"""

import ROOT
ROOT.gROOT.Macro("~/rootlogon.C")


##store one line with V, I, and time
class datentry:
    def __init__( self, line ):

        nums = [ float( e ) for e in line.split() ]
        
        self.v = nums[0]

        self.t = nums[2]

        self.i = nums[1]
       


def file2graph( fname ):
    """Read a file in, and make a TGraph of I v. V"""
    
    import re

    g237 = ROOT.TGraph()
    
    with open(fname) as f:

        line = f.readline()
        while re.match("Number of points : [0-9]+",line) is None:
            line = f.readline()

        line = f.readline() #reads the data line
        
        while re.match("[0-9]+/[0-9]+/[0-9]+.*",line) is None:

            d = datentry( line )

            n = g237.GetN()
            g237.SetPoint( n, d.v, d.i*1e9 )
            

            line = f.readline()

    return g237
    
if __name__ == "__main__":

    files = [ "data/IVtestAug29/iv.dat",
              ]

    c237 = ROOT.TCanvas()
        
    grs = []

    #loop over each file in the list
    for i,f in enumerate(files):
        
        #make a graph; save it to a list so the reference is not lost causing ROOT to delete it
        g237 = file2graph(f)
        grs.append( g237 )
        
        
        g237.SetLineColor(ROOT.myColSet2[i])
        g237.SetMarkerColor(ROOT.myColSet2[i])

        c237.cd()

        #Draw the graph in the canvas
        if i == 0:
            g237.Draw("ALP")
            g237.GetXaxis().SetTitle("Bias [V]")
            g237.GetYaxis().SetTitle("Current [nA]")
        else:
            g237.Draw("SAME LP")

    #save it
    c237.SaveAs("plots/iv.pdf")

    
