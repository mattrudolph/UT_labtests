
import ROOT

import plot_iv


## Each string should give the path to a file name with an IV curve from which to derive a resistance
filelist = [ 'data/IVtestAug29/iv.txt',
]

## Linear fit
fline = ROOT.TF1("fline","[0] + [1]*x", -1,1)

##histogram summarizing resistances -- modify the binning based on the resistances expected
hres = ROOT.TH1F("hres","Resistance histogram; Resistance [M#Omega]", 10, 0, 1)

graphs = []

for i,f in enumerate(filelist):

    ##resulting graph is in units of nanoamp v. volt
    graph = plot_iv.file2graph( f )
    graphs.append(graph)
    
    graph.Fit( fline )

    slope = fline.GetParameter(1)

    ## unit will be gigaOhm (  V/nA = 10^9 Ohm ), scale to mega ohm with factor of 1000
    res = 1.0e3/slope
    print "Resistance",i,"=",res
    ##
    hres.Fill( res )


canv = ROOT.TCanvas()
hres.Draw()
canv.SaveAs("resistance.png")

graphs[0].Draw("AP")
graphs[0].GetXaxis().SetTitle("Bias [V]")
graphs[0].GetYaxis().SetTitle("Current [nA]")
for g in graphs[1:]:
    g.Draw("SAME P")

canv.SaveAs("res_ivs.png")
