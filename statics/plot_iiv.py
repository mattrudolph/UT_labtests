

"""plot_iiv.py
Stuff to parse and plot info from IIV data files

File has a header that is something like:

C:\Documents and Settings\lhcb\Desktop\Sept2016\SingleStripTestAug26\default_after3hrwarmup.dat
8/26/2016
4:14:41 PM
#RH= 39.00
Temp (C)= 19.40
COMMENTS : 
========== DATA : V --  (Time_237  I_237)*Nb of counts/profile --  (Time_2002  I_2002)*Nb of counts/profile --  min max avg err I_237 --  min max avg err I_2002  ==========
Number of points : 21
Number of counts per voltage value : 3

Then each of the next lines is data.  At the end are more lines like :

8/26/2016
4:16:37 PM
RH= 39.00
Temp (C)= 19.40
"""

import ROOT
ROOT.gROOT.Macro("~/rootlogon.C")

class datentry:
    """Basic class to store the IIV readings for voltage, K237 and K2002"""
    def __init__( self, line ):

        nums = [ float( e ) for e in line.split() ]
        
        self.v = nums[0]

        self.avg2002 = nums[-2]
        self.err2002 = nums[-1]

        self.avg237 = nums[-6]
        self.err237 = nums[-5]

        pts = nums[ 1 : -8 ]
        if len(pts) % 4 != 0:
            print "Error wrong number of points"
            
        npts = len(pts) / 4
        self.i237 = [ pts[i] for i in range(1, npts*2, 2) ]
        self.i2002 = [ pts[i] for i in range( npts*2+1, npts*4, 2) ]
        


def file2graphs( fname ):
    """Read in the file with path fname, then create two TGraphErrors with the K237 and K2002 averages v. voltage"""
    import re

    g237 = ROOT.TGraphErrors()
    g2002 = ROOT.TGraphErrors()
    
    with open(fname) as f:

        line = f.readline()
        while re.match("Number of points : [0-9]+",line) is None:
            line = f.readline()

        line = f.readline() #reads the counts per voltage line
        line = f.readline() #reads the data line
        
        while re.match("[0-9]+/[0-9]+/[0-9]+",line) is None:

            d = datentry( line )

            n = g237.GetN()
            g237.SetPoint( n, d.v, d.avg237*1e9 )
            g237.SetPointError( n, 0, d.err237*1e9 )
            g2002.SetPoint( n, d.v, d.avg2002*1e9 )
            g2002.SetPointError( n, 0, d.err2002*1e9 )
            
            # print d.v, d.avg237, d.err237, d.avg2002, d.err2002
            # print d.i237
            # print d.i2002

            line = f.readline()

    return g237, g2002
    
if __name__ == "__main__":

    files = [ "data/iiv_tests_aug26/default_after3hrwarmup.dat",
              "data/iiv_tests_aug26/fix_1muA_range.dat",
              "data/iiv_tests_aug26/fix_range_and_50hztime.dat",
              "data/iiv_tests_aug26/fix_range_and_60hztime.dat"
              ]

    c237 = ROOT.TCanvas()
    c2002 = ROOT.TCanvas()
        
    grs = []
    for i,f in enumerate(files):
    
        g237, g2002 = file2graphs(f)
        grs.extend( [g237,g2002] )
        
        
        g237.SetLineColor(ROOT.myColSet2[i])
        g237.SetMarkerColor(ROOT.myColSet2[i])

        g2002.SetLineColor(ROOT.myColSet2[i])
        g2002.SetMarkerColor(ROOT.myColSet2[i])

        c237.cd()
        
        if i == 0:
            g237.Draw("ALP")
            g237.GetXaxis().SetTitle("Bias [V]")
            g237.GetYaxis().SetTitle("Current [nA]")
        else:
            g237.Draw("SAME LP")
            
        c2002.cd()
        if i==0:
            g2002.Draw("ALP")
            g2002.GetXaxis().SetTitle("Bias [V]")
            g2002.GetYaxis().SetTitle("Current [nA]")
            g2002.GetYaxis().SetRangeUser(0,2)
        else:
            g2002.Draw("SAME LP")


    c237.SaveAs("plots/iv237.pdf")
    c2002.SaveAs("plots/iv2002.pdf")
    
