
import ROOT
ROOT.gROOT.Macro("~/rootlogon.C")
import plot_iiv

c237 = ROOT.TCanvas()
c2002 = ROOT.TCanvas()


fs = ["data/InterStripResAug31/bias50resis130-129.dat",
      # "data/InterStripResAug31/bias300resis130-129.dat",
       "data/InterStripResAug31/bias50resis256-255.dat",
       "data/InterStripResAug31/bias300resis256-255.dat",
      ]
labs = ["130-129 50V", "256-255 50V", "256-255 300V"]

g237s = []
g2002s = []

func = ROOT.TF1("func"," [0] + [1]*x",-1,1)
txt = ROOT.TLatex()
leg = ROOT.TLegend(0.2, 0.65, 0.45, 0.9)
for i,fn in enumerate(fs):
    g237, g2002 = plot_iiv.file2graphs(fn)

    g237.SetLineColor(ROOT.myColSet2[i])
    g237.SetMarkerColor(ROOT.myColSet2[i])

    g2002.SetLineColor(ROOT.myColSet2[i])
    g2002.SetMarkerColor(ROOT.myColSet2[i])
    g2002.SetMarkerStyle(21)

    txt.SetTextColor(ROOT.myColSet2[i])
    
    g237s.append(g237)
    g2002s.append(g2002)

    leg.AddEntry( g237, labs[i], "L")
    
    if i==0:
        c237.cd()
        g237.Draw("ALP")
        g237.GetXaxis().SetTitle("Bias [V]")
        g237.GetYaxis().SetTitle("Current [nA]")

        g237.GetYaxis().SetRangeUser(-400,400)

        g237.Fit(func)

        slope = func.GetParameter(1)
        res = 1.0e3/slope
        reserr = res * ( func.GetParError(1)/slope )
        txt.DrawLatex( 0.2, -200, "R = {:.2f} #pm {:.2f} M#Omega".format(res, reserr) )
        
        
        c2002.cd()
        g2002.Draw("ALP")
        g2002.GetXaxis().SetTitle("Bias [V]")
        g2002.GetYaxis().SetTitle("Current [nA]")

        g2002.GetYaxis().SetRangeUser(0,2)

        g2002.Fit(func)

        slope = func.GetParameter(1)
        res = 1.0e0/slope
        reserr = res * ( func.GetParError(1)/slope )
        txt.DrawLatex( 0, 1.4, "R = {:.2f} #pm {:.2f} G#Omega".format(res, reserr) )
    else:
        c237.cd()
        g237.Draw("SAME LP")

        g237.Fit(func)

        slope = func.GetParameter(1)
        res = 1.0e3/slope
        reserr = res * ( func.GetParError(1)/slope )
        txt.DrawLatex( 0.2, -200 - 70*i, "R = {:.2f} #pm {:.2f} M#Omega".format(res, reserr) )
        
        c2002.cd()
        g2002.Draw("SAME LP")
        g2002.Fit(func)

        slope = func.GetParameter(1)
        res = 1.0e0/slope
        reserr = res * ( func.GetParError(1)/slope )
        txt.DrawLatex( 0, 1.4 - 0.2*i, "R = {:.2f} #pm {:.2f} G#Omega".format(res, reserr) )



c237.cd()
leg.Draw()
c237.SaveAs("plots/res237_2.pdf")
c2002.cd()
leg.Draw()
c2002.SaveAs("plots/res2002_2.pdf")
