
"""
Create an aliscan cfg file to scan the strips perpendicularly

It will scan the interstrip region with a width of 100micron, then move 90 micron to get to the next interstrip region
"""

##Set these and run to create a perpendicular scan program
relabs = 1
xstart=109455
ystart=105761
nscans=124


with open('ScanPerp.cfg','w') as f:
    f.write('{0} {1} {2}\n'.format(relabs,xstart,ystart))
    for i in range(0,nscans):
        f.write('10 0 10\n1  0 90\n')
