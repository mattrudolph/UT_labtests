
ALISCAN INSTRUCTIONS
--------------------

0.  Make sure alibava-gui is running and everything is setup BEFORE you start aliscan.exe

1.  When aliscan starts it will ask for a sensor name.  This defines the output directory in ~/data/${sensorname}/LaserScan/.  Each scan done will automatically create a new output directory with a number one higher than the highest in LaserScan/

2.  Next it will ask for a description string to put in the output file -- this is just to help you remember what the runs were.  SPACES NOT SUPPORTED AT THIS TIME.

3.  Then it will ask you if you want to load a program file.  If doing a long run, create a .cfg file with the movement instructions to load:
    a.  First line should be like "1 xpos ypos" -- 1 for absolute (use 0 for relative coordinates) then xpos and ypos are the place to start in unidex steps
    b.  Program will make a pedestal and 1 run at the starting position
    c.  Then it will follow your movement program, taking a laser run after each step
    d.  Program line looks like "nrepeat xsteps ysteps" -- you can repeat the same step nrepeat times.  Each time it will move xsteps and ysteps and then scan

4.  Otherwise, you can manually enter simple programs involving only one kind of step.  This is good for quick tests like when you are finding initial positioning.

5.  In the output directory, aliscan will create a console.out with a log of the description string, the program used, and printouts from each run of the scan.  This can be used to find the x,y location of each run.

6.  There will be one pedestal .dat file created

7.  Each run will have a separate .dat file, numbered in order.

8.  See UT_labtests/ALIPROC_README and individual example scripts for analysis help


TROUBLESHOOTING
---------------

**If getting OS error or some other error as soon as you start try:
   lsmod | grep gpib 
to check if the kernel module is loaded

   gpib_config --minor 0
to make sure the card is setup.  You might have to sudo this command (and if so I think you will need to use its full path -- find it with `which gpib_config`)

**If alibava board stops responding at some point during a long scan, don't have a solution other than hard reset it and start off where it failed (usually you can see this from the size of the data files being written)