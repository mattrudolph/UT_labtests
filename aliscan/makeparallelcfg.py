
"""
Create an aliscan cfg file to scan the strips perpendicularly

It will scan the interstrip region with a width of 100micron, then move 90 micron to get to the next interstrip region
"""

##Set these and run to create a perpendicular scan program
relabs = 1
xbottom=61843
xtop=157975
ytop=118868

#Bottom edge pattern
bnum=10
bstepx=10
badjx=-248
badjy=5

##Top edge pattern
tnum=10
tstepx=10
tadjx=-4
tadjy=2

#Main step pattern to get the slope of the sensor right
xstep=-52
ystep=1
mult=20 ##~ 1mm
iters=92

with open('ScanParallel.cfg','w') as f:
    f.write('{0} {1} {2}\n'.format(relabs,xtop,ytop))
    #scan the top
    f.write('{0} -{1} 0\n'.format(tnum,tstepx))
    #adjust for main line
    f.write('1 {0} {1}\n'.format(tadjx,tadjy))
    ##iterate the strip
    f.write('{0} {1} {2}\n'.format(iters, xstep*mult, ystep*mult))
    ##adjsut for bottom
    f.write('1 {0} {1}\n'.format(badjx,badjy))
    ##bottom scan
    f.write('{0} -{1} 0\n'.format(bnum,bstepx))

        
