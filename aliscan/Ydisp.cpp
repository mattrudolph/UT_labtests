#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <math.h>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <bitset>
#include <vector>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>


#include <gpib/ib.h>

void GPIBcommand(int Dev, std::string command); //give the command to the stage (consult manual for valid commands) no error checking of buffer cleaning!!
template<typename tocon> std::string ToString(tocon x);  //function used to convert some type tocon (int, float etc) to a standard string
void resetGPIBDevice(int dev); //resets the unidex necessary to avoid errors 
std::vector<double> ReadUnidexPosition(int Dev);//reading the position off unidex

#define BDINDEX               0     // Board Index
#define PRIMARY_ADDR_OF_DMM   05     // Primary address of device
#define NO_SECONDARY_ADDR     0     // Secondary address of device
#define TIMEOUT               T1000s  // Timeout value = 1000 seconds
#define EOTMODE               1     // Enable the END message
#define EOSMODE               0     // Disable the EOS mode

int main(int argc, char **argv)
{
  if ( argc < 2 ) {
    printf("Usage:  Ydisp value\n");
    exit(0);
  }
  int Ysteps = atoi(argv[1]);
  printf("Move %d steps in Y direction\n", Ysteps);


  char ErrorMnemonic[29][5] = { "EDVR", "ECIC", "ENOL", "EADR", "EARG",
				"ESAC", "EABO", "ENEB", "EDMA", "",
				"EOIP", "ECAP", "EFSO", "", "EBUS",
				"ESTB", "ESRQ", "", "", "",
				"ETAB", "ELCK", "EARM", "EHDL", "",
				"", "EWIP", "ERST", "EPWR" }; //GPIB errors

  int Dev = ibdev(BDINDEX, PRIMARY_ADDR_OF_DMM, NO_SECONDARY_ADDR,TIMEOUT, EOTMODE, EOSMODE);//initialization of the device	
  //std::cout<<"Device " << Dev << " initialized" <<std::endl;

  //resetting the Unidex
  GPIBcommand(Dev, "C*");
  usleep(150000);
  resetGPIBDevice(Dev);
  GPIBcommand(Dev, "ATN*");
  resetGPIBDevice(Dev);
  //std::cout<<"UNIDEX RESET"<<std::endl;
  
  std::cout << "Initial position " << ReadUnidexPosition(Dev)[1] << std::endl;

  std::string cmdString = "I Y F10000 D ";
  cmdString=cmdString+ToString(Ysteps)+"*";
  std::cout << "Issuing command: "<< cmdString << std::endl;
  ibwrt(Dev, (cmdString.data()), cmdString.length() + 1);
  
  std::cout << "Final position " << ReadUnidexPosition(Dev)[1] << std::endl;

  return 0;	
}

template<typename tocon> std::string ToString(tocon x)
{
  std::ostringstream result;
  result << x;
  return result.str();
}

void GPIBcommand(int Dev, std::string command)//generic command input to the unidex see programming manual for valid strings
{
  ibwrt(Dev, (char*)(command.data()), command.length() + 1);
}

void resetGPIBDevice(int dev) //resets the unidex necessary to avoid errors
{
  //std::thread t1(Play,1);
  //t1.detach();
  std::string cmdString = "7F";
  ibwrt(dev, (cmdString.data()), cmdString.length() + 1);
}

std::vector<double> ReadUnidexPosition(int Dev)//reading the position off unidex
{
  //std::cout << "GETTING POSITION" << std::endl;
  std::vector<double> Position;
  std::string cmdString = "Px";
  char outputx[100];
  char outputy[100];

  for (int i = 0; i < 100; i++)
    {
      outputx[i] = 'a';
      outputy[i] = 'a';
    }

  //std::cout<<"ReadUnidex Write 1" <<std::endl;
  ibwrt(Dev, (cmdString.data()), cmdString.length() + 1);
  //std::cout<<"ReadUnidex Read 1" <<std::endl;
  ibrd(Dev, outputx, 10000);
  //std::cout<<"ReadUnidex Done Read1" <<std::endl;

  std::string xpos = "";

  int j = 0;
  while (outputx[j] != 'a')
    {
      xpos = xpos + outputx[j];
      j++;
    }

  cmdString = "Py";
  //std::cout<<"ReadUnidex Write 2" <<std::endl;
  ibwrt(Dev, (cmdString.data()), cmdString.length() + 1);
  //std::cout<<"ReadUnidex Read 2" <<std::endl;
  ibrd(Dev, outputy, 10000);

  std::string ypos = "";

  j = 0;
  while (outputy[j] != 'a')
    {
      ypos = ypos + outputy[j];
      j++;
    }
  double x = strtod(xpos.c_str(),NULL);
  double y = strtod(ypos.c_str(),NULL);

  Position.push_back(x);
  Position.push_back(y);

  return Position;
}
