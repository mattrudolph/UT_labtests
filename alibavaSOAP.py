#!/usr/bin/env python
"""
Basic methods to communicate via SOAP with the Alibava gui.
It defines the special types: DAQParam and Status
It also defines a couple of conveniece classes. Ones is Hist, that helps
to access the data in form of histograms and Alibava.that encapsulates the 
SOAP commands
"""
import base64
import struct
import cStringIO
from SOAPpy import SOAPProxy
from SOAPpy import WSDL
import SOAPpy
import traceback
try:
    import ROOT
    def Hst2Root(H, hnam):
        OO = ROOT.gROOT.FindObject(hnam)
        if OO:
            OO.Delete()
    
        h1 = ROOT.TH1D(hnam, hnam, H.nx, H.xmin, H.xmax )
        for ib in range(0, H.nx):
            h1.SetBinContent(ib+1, H.buff[ib])
    
        return h1    
except ImportError:
    def Hst2Root(H, hnam):
        return H

root_objects=[None for i in range(0,5)]
    

#SOAPpy.Config.debug = 1
ALIBAVA_NS="http://alibavasystems.com/alibava/"

def load_wsdl():
    """ Loads the wsdl file from the server and prints 
        information about it
    """
    url = 'http://localhost:20000/alibava'
    
    # just use the path to the wsdl of your choice
    wsdlObject = WSDL.Proxy(url + '?wsdl')
    print 'Available methods:'
    for method in wsdlObject.methods.keys() :
        print method
        ci = wsdlObject.methods[method]
        # you can also use ci.inparams
        for param in ci.outparams :
            # list of the function and type 
            # depending of the wsdl...
            print param.name.ljust(20) , param.type
        print
        
        
"""
    SOAPpy does not handle properly complex objects until version 0.12.
    For earlier versions we have to modify the behaviour of SOAPbuilder
    and add the methods that dump those objects.
"""
# modify SOAPBuilder
# This is needed for SOAPpy versions before 0.12, otherwise, the class daqParam can handle it
def dump_DAQParam(self, obj, tag, typed = 1, ns_map = {}):
    """ This is a helper function for SOAPpy that will construct
        the request for a ParameterValue.
    """
    if SOAPpy.Config.debug: print "In dump_DAQParam. tag=", tag
    tag = tag or "daqParam"
    tag = SOAPpy.wstools.XMLname.toXMLname(tag) # convert from SOAP 1.2 XML name encoding

    id = self.checkref(obj, tag, ns_map)
    if id == None:
        return
    
    try: a = obj._marshalAttrs(ns_map, self)
    except: a = ''

    try:
        ns = ns_map[ALIBAVA_NS]
        tnm = 'xsi:type="%s:DAQParam"' % (ns)
    except KeyError:
        tnm = ''
    
    self.out.append('<%s %s>\n' % (tag, tnm))
    self.out.append('<runType xsi:type="xsd:int">%d</runType>\n' % obj.runType)
    self.out.append('<numEvents xsi:type="xsd:int">%d</numEvents>\n' % obj.numEvents)
    self.out.append('<sampleSize xsi:type="xsd:int">%d</sampleSize>\n' %obj.sampleSize)
    self.out.append('<nbin xsi:type="xsd:int">%d</nbin>' % obj.nbin)
    self.out.append('<xmin xsi:type="xsd:double">%f</xmin>' % obj.xmin)
    self.out.append('<xmax xsi:type="xsd:double">%f</xmax>' % obj.xmax)
    self.out.append('</%s>\n' % tag)
    
    
# modify SOAPBuilder
# This is needed for SOAPpy versions before 0.12, otherwise, the class daqParam can handle it
def dump_parValue(self, obj, tag, typed = 1, ns_map = {}):
    if SOAPpy.Config.debug: print "In dump_parValue. tag=", tag
    tag = tag or "daqParam"
    tag = SOAPpy.wstools.XMLname.toXMLname(tag) # convert from SOAP 1.2 XML name encoding

    id = self.checkref(obj, tag, ns_map)
    if id == None:
        return
    
    try: a = obj._marshalAttrs(ns_map, self)
    except: a = ''

    try:
        ns = ns_map[ALIBAVA_NS]
        tnm = 'xsi:type="%s:DAQParam"' % (ns)
    except KeyError:
        tnm = ''
    
    self.out.append('<%s %s>\n' % (tag, tnm))
    self.out.append('<name xsi:type="xsd:string">%s</name>\n' % obj.name)
    self.out.append('<value xsi:type="xsd:string">%s</value>\n' % obj.value)
    self.out.append('</%s>\n' % tag)    
    
def dump_int(self, obj, tag, typed = 1, ns_map = {}):
    """ SOAPpy insists in using std::integer for the int, while we use xsd:int
        This will change the way in which SOAPbuilder does the job
    """
    if SOAPpy.Config.debug: print "In my_dump_int."
    self.out.append(self.dumper(None, 'int', obj, tag, typed,
                                     ns_map, self.genroot(ns_map)))
    
def funcToMethod(func,clas,method_name=None):
    """ Helper function to add dynamically methods to a class
    """
    import new
    method = new.instancemethod(func,None,clas)
    if not method_name: method_name=func.__name__
    clas.__dict__[method_name]=method
    
funcToMethod(dump_DAQParam, SOAPpy.SOAPBuilder, "dump_daqParam")
funcToMethod(dump_parValue, SOAPpy.SOAPBuilder, "dump_parValue")        
funcToMethod(dump_int, SOAPpy.SOAPBuilder, "dump_int")        


(PEDESTAL, RADIOACTIVE, LASER, LASERSCAN, CALIBRATION, CHARGE_SCAN) = range(0,6)

class parValue(dict):
    def __init__(self, par_name, par_value):
        self['name'] = par_name
        self['value'] = par_value
        
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError, name

class daqParam(dict):
    def __init__(self, run_type, num_events, sample_size, nbin=1024, xmin=-512.0, xmax=512.0):
        self['runType'] = int(run_type)
        self['numEvents'] = int(num_events)
        self['sampleSize'] = int(sample_size)
        self['nbin'] = int(nbin)
        self['xmin'] = float(xmin)
        self['xmax'] = float(xmax)
    
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError, name 
            

        
        
class Status(object):
    """ The status of a DAQObject.
    """
    attr_list = {'state': lambda x:x, 
                 'time': lambda x:x, 
                 'nexpected': int, 
                 'ntrigger': int, 
                 'rate': float, 
                 'run_type': lambda x:x, 
                 'value': float}
    def __init__(self, response):
        for attr, cast in Status.attr_list.items():
            #print attr, getattr(response, attr)
            setattr(self, attr, cast(getattr(response, attr)))
            
    def __str__(self):
        rate = self.rate;
        rate_units=" Hz";
        thr_units = " Mb/s";

        if  rate>1.e6:
            rate /=1.e6
            rate_units="MHz"
   
        elif rate>1000.0: 
            rate /=1000.
            rate_units = "kHz"

        out = "Evts %7d time %s rate %8.1f %s value %f" % \
            (self.ntrigger, self.time, rate, rate_units, self.value)

        return out

        
class Hst(object):
    """ This is a histogram
    """
    def __init__(self, nx, xmin, xmax, buff=None):
        self.nx = nx
        self.xmin = xmin
        self.xmax = xmax
        if buff:
            self.buff = buff
        else:
            self.buff = [ 0.0 for i in range(0, nx) ]
                        
    def __str__(self):
        ss = "Nbin: %d xmin %f xmax %f\n" % (self.nx, self.xmin, self.xmax)
        i=0
        for val in self.buff:
            ss += "%d - %f\n" %(i, val)
            i += 1
        return ss




def decode_data(data, use_root=True):
    """
    This method decodes the data received from alibava-gui and 
    returns a list of histograms.
    """
    val = cStringIO.StringIO(base64.decodestring(data))
    
    fmt ='i'
    sz = struct.calcsize(fmt)
    v = struct.unpack(fmt, val.read(sz))
    nhst = v[0]
    hst_list = []
    
    for i in range(0, nhst):
        fmt = 'i'
        sz = struct.calcsize(fmt)
        nbin = struct.unpack(fmt, val.read(sz))[0]

        fmt = 'd'
        sz = struct.calcsize(fmt)
        xmin = struct.unpack(fmt, val.read(sz))[0]

        fmt = 'd'
        sz = struct.calcsize(fmt)
        xmax = struct.unpack(fmt, val.read(sz))[0]
        
        fmt = "%dd" % nbin
        sz = struct.calcsize(fmt)
        hst = struct.unpack(fmt, val.read(sz))
        
        H = Hst(nbin, xmin, xmax, hst)
        if use_root:
            H = Hst2Root( H, "__hst_%d__" % i)
        root_objects[i] = H
        
        hst_list.append( H )
    
    return hst_list


class Alibava(object):
    """ Proxy to talk to Alibava
        It encapsulates the SOAP client
    """
    def __init__(self, host, port, use_root=True):
        self.server = SOAPpy.SOAPProxy("http://%s:%d/alibava" %(host, port), namespace=ALIBAVA_NS, noroot=1)
        self.use_root = use_root
        
    def getStatus(self):
        return Status( self.server.getStatus() )
    
    def stopRun(self):
        self.server.stopRun(0)
        
    def startPedestalRun(self, nevt, nbin=512, xmin=-512, xmax=-512, nsample=100):
        R = self.server.startRun(daqParam=daqParam(PEDESTAL, nevt, nsample, nbin, xmin, xmax));
        return decode_data(R, self.use_root)
    
    def startSourceRun(self, nevt, nbin=512, xmin=-512, xmax=-512, nsample=100):
        R = self.server.startRun(daqParam=daqParam(RADIOACTIVE, nevt, nsample, nbin, xmin, xmax));
        return decode_data(R, self.use_root)
    
    def startLaserRun(self, nevt, nbin=512, xmin=-512, xmax=-512, nsample=100):
        R = self.server.startRun(daqParam=daqParam(LASER, nevt, nsample, nbin, xmin, xmax));
        return decode_data(R, self.use_root)
    
    def startCalibrationRun(self, nevt_per_point=50, npts=10, vfrom=0, vto=30000, is_charge=True):
        if is_charge:
            cal_type = 1
        else:
            cal_type = 0
        R = self.server.startRun(daqParam=daqParam(CALIBRATION, nevt_per_point, cal_type, npts, vfrom, vto));
        return decode_data(R, self.use_root)
    
    def startChargeScan(self, nevt_per_point=100, npts=10, vfrom=0, vto=60000):
        R = self.server.startRun(daqParam=daqParam(CHARGE_SCAN, nevt_per_point, 1, npts, vfrom, vto))
        return decode_data(R, self.use_root)
    
    def startLaserSync(self, nevt_per_point=50, npts=10, vfrom=0, vto=512):
        R = self.server.startRun(daqParam=daqParam(LASERSCAN, nevt_per_point, 0, npts, vfrom, vto));
        return decode_data(R, self.use_root)
    
    def setDataFile(self, fnam):
        self.server.setDataFile(dataFile=fnam)
        
    def setParameter(self, parname, parvalue):
        self.server.setParameter(parValue=parValue(parname, parvalue))

