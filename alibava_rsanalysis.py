
"""alibava_rsanalysis.py: Analyze the output from alibava_rstest.py for the directory specified on command line.  Uses aliproc as a base for the pedestal and noise calculation, but modifies the loop_file and proc_event methods to include the tdc time."""

import os
import re

from pythonbase.aliproc import aliproc
import ROOT
import pythonbase.pedestal as pedestal
from pythonbase.alibavareader import prepfile, next_event, read_block, parse_datablock
from pythonbase.clusterizer import clusterize,clusterpos

class rsanalysis(aliproc):
    def __init__(self,*indirs):
        aliproc.__init__(self,*indirs)
        self.datfiles = []
        for indir in self.indirs:
            self.datfiles.extend( sorted([os.path.join(indir,f) for f in os.listdir(indir) if re.match("rs_data.dat",f) is not None]))

        self.nfiles = len(self.datfiles)
            
        self.thresh = 4 #5 times noise

        #all the ROOT histograms to fill
        self.allsig = ROOT.TH1F("allsig","all signals; ADC",50,0,500)
        self.tdc0sig = ROOT.TH1F("sigtdc0","signals with tdc 0; ADC",100,0,500)
        self.allsn  = ROOT.TH1F("allsn","all s over n; S/N",100,0,100)
        self.hitmap = ROOT.TH1F("hitmap","hit channels; Chan. #",256,-0.5,255.5)
        self.nhit   = ROOT.TH1F("nhit", "num hit per event; N_{hit}",10,-0.5,9.5)
        self.hitsize= ROOT.TH1F("hitsize","num strip per hit; N_{strip}",5,0.5,5.5)
        self.hiteta = ROOT.TH1F("hiteta","hit eta; #eta",100,0,1)
        self.tdc = ROOT.TH1F("tdc","tdc time; TDC", 101, -0.5, 100.5)
        self.sigvtdc = ROOT.TProfile("sigvtdc","sig v tdc time; TDC; ADC",101, -0.5, 100.5)

        self.sigreg1 = ROOT.TH1F("sigreg1","signal region 1;ADC", 100,0,500)
        self.sigreg2 = ROOT.TH1F("sigreg2","signal region 2;ADC", 100,0,500)

        self.cmshifts = [ ROOT.TH1F("cmshift{0}".format(i),"CM shift; Common mode shift [ADC]",200,-500,500) for i in range(8)]
        
        
    def begin_file(self,j):
        return

    def end_file(self,j):
        return
    
    def proc_event(self,ev):
        tdc = ev[0][-1]
        data = ev[-1]
        
        #subtract pedestal from each channel for this event
        pedsub =  [ data[i] - self.ped[i] for i in range(len(data))]
        #subtract the common mode noise from each block of 32 channels
        cmsub,cmshifts = pedestal.commonmode( pedsub )

        for i,s in enumerate(cmshifts):
            self.cmshifts[i].Fill(s)
        
        #make a list of clusters of strips ( each cluster is just a list of channel numbers that are included)
        clusters = clusterize( cmsub, self.noise, high=self.thresh, low=self.thresh-1, polarity=self.polarity )

        #fill histogram of clusters per event
        self.nhit.Fill(len(clusters))

        #loop over all the clusters
        for c in clusters:

            #total signal and signal/noise
            totsig=0
            totsn=0

            #keep track if the cluster is in a region of interest 
            reg1 = False
            reg2 = False

            #loop over every strip in the cluster
            for strip in c:
                #add up the signals
                totsig += self.polarity*cmsub[strip]
                totsn += self.polarity*cmsub[strip]/self.noise[strip]

                #check if the cluster is in a region of interest 
                if(strip >= 70 and strip < 75):
                    reg1 = True
                elif(strip >= 80 and strip < 85):
                    reg2 = True

            #fill the time information
            self.tdc.Fill(tdc)
            self.sigvtdc.Fill(tdc,totsig)

            if tdc < 1:
                self.tdc0sig.Fill(totsig)
            
            #cut on time region with most signal
            if (tdc < 20) or (tdc > 40):
                continue

            #for two strip clusters, plot fraction of charge on the right strip
            if len(c) == 2:
                self.hiteta.Fill( cmsub[c[1]]/(cmsub[c[0]]+cmsub[c[1]]) )
            #fill histograms
            self.allsig.Fill(totsig)
            self.allsn.Fill(totsn)
            self.hitsize.Fill(len(c))

            pos = clusterpos(c,cmsub)
            self.hitmap.Fill(clusterpos( c, cmsub))

            if(reg1):
                self.sigreg1.Fill(totsig)
            elif(reg2):
                self.sigreg2.Fill(totsig)

    def sum_plots(self):
        #make sure canvas is active
        self.canv.cd()

        #draw on the canvas, then save
        for h in [self.allsig, self.tdc0sig, self.allsn, self.hitmap, self.nhit, self.hitsize, self.hiteta, self.tdc, self.sigvtdc]:
            h.Draw()
            self.saveplot(h.GetName(), h)


        # ##Langaus fit
        adc = ROOT.RooRealVar("adc","adc",0,500)
   
        # mg = ROOT.RooRealVar("mg","mg",0) 
        # mean = self.allsig.GetXaxis().GetBinCenter( self.allsig.GetMaximumBin() )
        # sigma = 10
        # sg = ROOT.RooRealVar("sg","sg",sigma,0.1*sigma,5.*sigma) 
        # gauss = ROOT.RooGaussian("gauss","gauss",adc,mg,sg) 

        # ml = ROOT.RooRealVar("ml","mean landau",mean,mean-5*sigma,mean+5*sigma) 
        # sl = ROOT.RooRealVar("sl","sigma landau",1,0.,100) 
        # landau = ROOT.RooLandau("lx","lx",adc,ml,sl) 

        # adc.setBins(5000,"cache") 

        # lxg = ROOT.RooFFTConvPdf("lxg","landau (X) gauss",adc,landau,gauss)
        
        data = ROOT.RooDataHist("dh","dh",ROOT.RooArgList(adc), self.allsig)

        # lxg.fitTo(data)

        frame = adc.frame()
        data.plotOn(frame)
        # lxg.plotOn(frame)
        frame.Draw()
        # txt = ROOT.TLatex()
        # txt.DrawLatexNDC(0.6,0.5,"#mu = {:.1f} #pm {:.1f}".format( ml.getVal(), ml.getError()))

        # ##mean noise of channels
        # mn = 0
        # for n in self.noise[:128]:
        #     mn += n
        # txt.DrawLatexNDC(0.6,0.45,"n #approx {:.1f}".format( float(mn)/128.0))
        
        self.saveplot("fitsig")
            
        self.sigreg2.SetLineColor(ROOT.kRed)
        self.sigreg2.Draw()
        self.sigreg1.SetLineColor(ROOT.kBlue)
        self.sigreg1.Draw("SAME")
        self.saveplot( "sigregions" )
        self.outfile.cd()
        self.sigreg1.Write()
        self.sigreg2.Write()

        txtsig=ROOT.TPaveText(0.7,0.22,0.9,0.9,"NDC")
        txtsig.SetBorderSize(0)
        txtsig.SetFillColor(0)
        txtm=ROOT.TPaveText(0.2,0.22,0.4,0.9,"NDC")
        txtm.SetBorderSize(0)
        txtm.SetFillColor(0)

        for i,h in enumerate(self.cmshifts):
            h.SetStats(False)
            h.SetLineColor(i+1)
            txtm.AddText( "#mu_{{ {0} }} = {1:.2f}".format(i,h.GetMean()) )
            txtsig.AddText( "#sigma_{{ {0} }} = {1:.2f}".format(i, h.GetStdDev() ) )

            if i==0:
                h.Draw()
            else:
                h.Draw("SAME")
                
        txtm.Draw()
        txtsig.Draw()
        self.saveplot("cmshifts")
        
if __name__== '__main__':
    import sys

    if len(sys.argv) < 2:
        print "Pass the directory to process as the first argument"
        sys.exit()

    indir = sys.argv[1]

    sys.argv.append('-b')
    
    ROOT.gROOT.Macro("~/rootlogon.C")

    plotter = rsanalysis(indir)

    plotter.plot_pedestal()
    
    plotter.loop_files()

    plotter.sum_plots()
