import ROOT


    
ROOT.gROOT.Macro("~/rootlogon.C")


v = [40.,70.,100.,150.,200.,250.,300.,350.,400.]
mu =[23.2, 25.6, 29.5, 37.7, 44.4, 52.3, 49.6, 53.4, 52.8 ]
muerr = [0.3, 0.4, 0.4, 0.5, 0.6, 0.9, 2.5, 1.4, 0.8]
n = [4.9, 4.7, 4.6, 4.5, 4.4, 4.4, 4.4, 4.4, 4.4]

grmu = ROOT.TGraphErrors()
grsn = ROOT.TGraphErrors()

for i in range(len(v)):
    grmu.SetPoint(i, v[i], mu[i])
    grmu.SetPointError(i, 0,muerr[i])

    grsn.SetPoint(i, v[i], mu[i]/n[i])
    grsn.SetPointError(i,0, muerr[i]/n[i])

grmu.SetLineColor(ROOT.myColSet2[0])
grmu.SetMarkerColor(ROOT.myColSet2[0])
grsn.SetLineColor(ROOT.myColSet2[1])
grsn.SetMarkerColor(ROOT.myColSet2[1])


c = ROOT.TCanvas()
c.SetRightMargin(0.16)
c.SetTicky(0)

grmu.Draw("AP")
grmu.GetXaxis().SetTitle("Bias [V]")
grmu.GetYaxis().SetTitle("Signal [ADC]")
grmu.GetYaxis().SetTitleColor(ROOT.myColSet2[0])
grmu.GetYaxis().SetAxisColor(ROOT.myColSet2[0])
grmu.GetYaxis().SetLabelColor(ROOT.myColSet2[0])

grmu.GetYaxis().SetRangeUser(0,60)

c.Update()

scale = ROOT.gPad.GetUymax()/15.

for i in range(len(v)):
    grsn.SetPoint( i, v[i], mu[i]*scale/n[i] )
    grsn.SetPointError( i, 0, muerr[i]*scale/n[i] )    

grsn.Draw("same P")
print ROOT.gPad.GetUxmax(),ROOT.gPad.GetUymin(), ROOT.gPad.GetUxmax(), ROOT.gPad.GetUymax()
axis = ROOT.TGaxis(ROOT.gPad.GetUxmax(),ROOT.gPad.GetUymin(),
         ROOT.gPad.GetUxmax(), ROOT.gPad.GetUymax(),0,15,510,"+L")
axis.SetLabelColor(ROOT.myColSet2[1])
axis.SetLineColor(ROOT.myColSet2[1])
axis.SetTitleColor(ROOT.myColSet2[1])
axis.SetLabelSize( ROOT.lhcbTSize)
axis.SetTitleSize( 1.2*ROOT.lhcbTSize)
axis.SetTitle("S/N")
axis.Draw()
c.SaveAs("biasscan.png")
c.SaveAs("biasscan.pdf")
