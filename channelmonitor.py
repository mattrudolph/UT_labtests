#!/usr/bin/env python

"""Aliproc-based class to monitor raw values of particular user-specified channels in a run"""

from pythonbase.aliproc import aliproc
import ROOT
import pythonbase.pedestal as pedestal
from pythonbase.alibavareader import *

class channelmonitor(aliproc):
    def __init__(self,indir,channels):
        aliproc.__init__(self,indir)

        self.canv = ROOT.TCanvas()
        self.channels = channels

        self.pedsubs = []
        self.cmsubs = []
        for c in channels:
            self.pedsubs.append( ROOT.TH1F("pedsub%i" % c,"ped sub values",400,-100,300) )
            self.cmsubs.append( ROOT.TH1F("cmsub%i" % c,"CM sub values",400,-100,300) )


    def reset(self):
        for h in self.pedsubs:
            h.Reset()
        for h in self.cmsubs:
            h.Reset()

    def proc_event(self,ev):
        for i,c in enumerate(self.channels):
            pedsub = [ ev[2][k] - self.ped[k] for k in range( c - (c % 32), c - (c % 32) + 32 )]
            cms,_ = pedestal.commonmode(pedsub)
            
            self.pedsubs[i].Fill( pedsub[c % 32] )
            self.cmsubs[i].Fill( cms[c % 32] )

    def loop_pedestal(self):
        with open(self.indir+('pedestal.dat')) as f:
            (timestamp, runtype, info, pedestal, noise) = prepfile(f)
            
            ev = next_event(f)

            while ev is not None:

                self.proc_event(ev)
                ev = next_event(f)
                
    def save_plots(self,prefix):
        self.canv.cd()
        colors = [ 1, ROOT.kBlue, ROOT.kRed, ROOT.kGreen + 3]

        leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

        for i,c in enumerate(self.channels):
            h = self.pedsubs[i]
            h.SetMarkerStyle(20)
            h.SetLineColor(colors[i])
            h.SetMarkerColor(colors[i])

            leg.AddEntry( self.pedsubs[i], "Channel %i" % c, "L")
            if i==0:
                h.Draw()
                h.GetXaxis().SetTitle("ADC")
            else:
                h.Draw("SAME")

        leg.Draw()
        self.canv.SaveAs(self.plotdir+prefix+"_pedsub.pdf")

        for i,c in enumerate(self.channels):
            h = self.cmsubs[i]
            h.SetMarkerStyle(20)
            h.SetLineColor(colors[i])
            h.SetMarkerColor(colors[i])

            if i==0:
                h.Draw()
                h.GetXaxis().SetTitle("ADC")
            else:
                h.Draw("SAME")

        leg.Draw()
        self.canv.SaveAs(self.plotdir+prefix+"_cmsub.pdf")


if __name__ == "__main__":
    import sys

    if len(sys.argv) < 2:
        print "Pass the directory to process as the first argument"
        sys.exit()

    indir = sys.argv[1]

    sys.argv.append('-b')

    plotter = channelmonitor(indir, [252,253,254,255])

    plotter.loop_pedestal()
    plotter.save_plots("pedestal")
    plotter.reset()

    #runs = [102,103,104,105,106,107,286,287,288,289,290,291,292,293,294,295,296,297,298]
    runs = range(100,136)
    for i in runs:
        plotter.loop_file(i)
        plotter.save_plots("run%i"%i)
        plotter.reset()
